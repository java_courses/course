package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.common.marshaller.DateAdapter;
import com.dp9v.training_timetable.models.pojo.User;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 02.03.17.
 */
@XmlRootElement(name = "user")
public class UserXML implements XMLInterface {

	private UUID          identifier;
	private String        name;
	private String        email;
	private String        password;
	private java.sql.Date birthday;
	private boolean       isAdmin;
	private boolean       isActive;
	private UUID          token;
	private boolean       isNotif;

	public UserXML() {
	}

	public UserXML(User user) {
		this.identifier = user.getIdentifier();
		this.name = user.getName();
		this.email = user.getEmail();
		this.password = user.getPassword();
		this.birthday = user.getBirthday();
		this.isAdmin = user.getIsAdmin();
		this.isActive = user.getIsActive();
		this.token = user.getToken();
		this.isNotif = user.getGetNotifications();
	}


	@Override
	public Object getModel() {
		User res = new User();
		res.setIdentifier(identifier);
		res.setName(name);
		res.setEmail(email);
		res.setPassword(password);
		res.setBirthday(birthday);
		res.setIsAdmin(isAdmin);
		res.setIsActive(isActive);
		res.setToken(token);
		res.setGetNotifications(isNotif);
		return res;
	}


	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@XmlJavaTypeAdapter(DateAdapter.class)
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean admin) {
		isAdmin = admin;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public boolean isNotif() {
		return isNotif;
	}

	public void setNotif(boolean notif) {
		isNotif = notif;
	}

}
