package com.dp9v.training_timetable.controllers.listeners;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.common.utils.Mailer;
import com.dp9v.training_timetable.models.pojo.User;
import com.dp9v.training_timetable.services.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.List;

/**
 * Created by dpolkovnikov on 01.03.17.
 */
@WebListener()
@Component
public class AdminAuthListener implements HttpSessionAttributeListener {
	private UserService userService;


	@Override
	public void attributeAdded(HttpSessionBindingEvent event) {
		if (userService == null)
			obtainService(event);

		if(!event.getName().equals("isAdmin")) {
			return;
		}
		if(!(Boolean) event.getValue()){
			return;
		}
		List<User> users = null;
		try {
			users = userService.all();
			for(User user :users) {
				if(user.getGetNotifications()){
					Mailer.sendMessage("Произошла авторизация",
					                   "На вашем сайте была произведена авторизация пользователя с правами администратора",
					                   user.getEmail());
				}
			}
		}
		catch (UserServiceException e) {
		}
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent event) {

	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent event) {

	}

	private void obtainService(HttpSessionBindingEvent event) {
		HttpSession           session = event.getSession();
		WebApplicationContext ctx     = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
		userService = (UserService) ctx.getBean("userService");
	}
}
