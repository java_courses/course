package com.dp9v.training_timetable.common.loader.lists;

import com.dp9v.training_timetable.common.loader.models.UserXML;
import com.dp9v.training_timetable.models.pojo.User;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlRootElement(name = "User")
@XmlType(propOrder={"items"})
@XmlSeeAlso(UserXML.class)
public class Users extends AbstractModelList<UserXML> {
	public Users(ArrayList<UserXML> items) {
		super(items);
	}

	public void fillUsers(List<User> items)
	{
		for(User item: items) {
			this.putItem(new UserXML(item));
		}
	}

	public Users() {
		super();
	}

	public ArrayList<User> getModels(int begin, int count){
		ArrayList<User> users = new ArrayList<>(count);
		for(int i=begin; i<begin+count && i<this.getItems().size(); i++){
			users.add((User) getItems().get(i).getModel());
		}
		return users;
	}
}
