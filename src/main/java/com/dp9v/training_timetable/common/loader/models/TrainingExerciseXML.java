package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;
import com.dp9v.training_timetable.services.ExerciseService;
import com.dp9v.training_timetable.services.TrainingService;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 02.03.17.
 */
@XmlRootElement(name = "trainingExercise")
public class TrainingExerciseXML implements XMLInterface {

	private UUID    identifier;
	private UUID    training;
	private UUID    exercise;
	private double  counter;
	private boolean isDone;
	private String  comment;

	public TrainingExerciseXML(TrainingExercise trainingExercise) {
		this.identifier = trainingExercise.getIdentifier();
		this.training = trainingExercise.getTraining().getIdentifier();
		this.exercise = trainingExercise.getExercise().getIdentifier();
		this.counter = trainingExercise.getCounter();
		this.isDone = trainingExercise.getIsDone();
		this.comment = trainingExercise.getComment();
	}

	public TrainingExerciseXML() {
	}

	@Override
	public Object getModel() throws ServiceException {
		TrainingExercise res = new TrainingExercise();
		res.setIdentifier(identifier);
		res.setTraining(new TrainingService().getByIdentifier(training));
		res.setExercise(new ExerciseService().getByIdentifier(exercise));
		res.setCounter(counter);
		res.setIsDone(isDone);
		res.setComment(comment);
		return res;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public UUID getTraining() {
		return training;
	}

	public void setTraining(UUID training) {
		this.training = training;
	}

	public UUID getExercise() {
		return exercise;
	}

	public void setExercise(UUID exercise) {
		this.exercise = exercise;
	}

	public double getCounter() {
		return counter;
	}

	public void setCounter(double counter) {
		this.counter = counter;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean done) {
		isDone = done;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}


}
