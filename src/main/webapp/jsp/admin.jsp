<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--@elvariable id="user" type="java.util.List<com.dp9v.training_timetable.models.pojo.User>"--%>
<%--@elvariable id="exercises" type="java.util.List<com.dp9v.training_timetable.models.pojo.Exercise>"--%>
<%--@elvariable id="metrics" type="java.util.List<com.dp9v.training_timetable.models.pojo.Metric>"--%>

<rapid:override name="title">Список пользователей</rapid:override>

<rapid:override name="content">
    <div class="starter-template">
        <ul class="nav nav-tabs" id="adminTabs">
            <li class="active"><a href="#userTab" data-toggle="tab">Пользователи</a></li>
            <li><a href="#metricTab" data-toggle="tab">Метрики</a></li>
            <li><a href="#exercisesTab" data-toggle="tab">Упражнения</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="userTab">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ФИО</th>
                            <th>E-mail</th>
                            <th>День рождения</th>
                            <th>Админ</th>
                            <th>Активен</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${users}" var="user">
                            <tr>
                                <td><c:out value="${user.id}"/></td>
                                <td><c:out value="${user.name}"/></td>
                                <td><c:out value="${user.email}"/></td>
                                <td><c:out value="${user.birthday}"/></td>
                                <td>
                                    <input type="checkbox" name="isAdmin" class="ajaxCheckBox big-checkbox"
                                           data-id="<c:out value="${user.id}"/>"
                                           <c:if test="${user.isAdmin}">checked</c:if>>
                                </td>
                                <td><input type="checkbox" name="isActive" class="ajaxCheckBox big-checkbox"
                                           data-id="<c:out value="${user.id}"/>"
                                           <c:if test="${user.isActive}">checked</c:if>/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>

                <input type="checkbox" name="getNotifications"
                       class="ajaxCheckBox big-checkbox"
                       data-id="<c:out value="${currentUser.id}"/>"
                       <c:if test="${currentUser.getNotifications}">checked</c:if>/>
                Уведомлять об авторизации другихадминистраторов
            </div>
            <div class="tab-pane" id="metricTab">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${metrics}" var="metric">
                            <tr id="row${metric.id}" class="row-metric">
                                <td class="idValue"><c:out value="${metric.id}"/></td>
                                <td class="typeValue"><c:out value="${metric.type}"/></td>
                                <td>
                                    <button class="btn btn-rm" data-id="${metric.id}"
                                            data-link="<c:url value="/admin/removeMetric"/>">Удалить
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-metric-edit"
                                            data-version="${metric.version}">Редактировать
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="4">
                                <button class="btn btn-metric-create">Добавить</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="exercisesTab">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Информация</th>
                            <th>Метрика</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${exercises}" var="exercise">
                            <tr id="row${exercise.id}">
                                <td class="idValue"><c:out value="${exercise.id}"/></td>
                                <td class="nameValue"><c:out value="${exercise.name}"/></td>
                                <td class="infoValue"><c:out value="${exercise.info}"/></td>
                                <td class="metricValue" data-id="${exercise.metric.id}">
                                    <c:out value="${exercise.metric.type}"/>
                                </td>
                                <td>
                                    <button class="btn btn-rm" data-id="${exercise.id}"
                                            data-link="<c:url value="/admin/removeExercise"/>">Удалить
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-exercise-edit"
                                            data-version="${exercise.version}">Редактировать
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="6">
                                <button class="btn btn-exercise-create">Добавить</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="popups/exercisePopup.jsp" flush="true"/>
    <jsp:include page="popups/metricPopup.jsp" flush="true"/>
</rapid:override>


<%@ include file="/jsp/base.jsp" %>