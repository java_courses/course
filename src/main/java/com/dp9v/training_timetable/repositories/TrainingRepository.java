package com.dp9v.training_timetable.repositories;

import com.dp9v.training_timetable.models.entity.TrainingEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public interface TrainingRepository extends PagingAndSortingRepository<TrainingEntity, Long>{
	public final String TABLE_NAME = "trainings";
	TrainingEntity getByIdentifier(UUID identifier);
	List<TrainingEntity> findByDeleteTypeAndUser_Id(Integer deleteType, Long userId);
	List<TrainingEntity> findByDeleteTypeOrderByBeginTimeAsc(Integer deleteType);
}
