package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingExerciseServiceException;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dpolkovnikov on 25.02.17.
 */
@Service
public interface ITrainingExerciseService extends ServiceInterface<TrainingExercise> {

	TrainingExercise save(TrainingExercise model, String name) throws TrainingExerciseServiceException;

	Boolean checkAccess(Long id, String email) throws TrainingExerciseServiceException;

	List<TrainingExercise> findForTraining(Long id) throws TrainingExerciseServiceException;
}
