<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<rapid:override name="title">Список пользователей</rapid:override>

<rapid:override name="content">
    <div class="starter-template">
        <a class="btn btn-lg btn-primary" href="${pageContext.request.contextPath}/admin/">Панель администратора</a>
        <br>
        <br>
        <a class="btn btn-lg btn-warning" href="${pageContext.request.contextPath}/timetable/">Список тренировок</a>
    </div>
</rapid:override>


<%@ include file="/jsp/base.jsp" %>