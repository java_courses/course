package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.models.pojo.Metric;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 02.03.17.
 */
@XmlRootElement(name = "exercise")
public class ExerciseXML implements XMLInterface {

	private UUID      identifier;
	private String    name;
	private String    info;
	private MetricXML metric;

	public ExerciseXML() {
	}

	public ExerciseXML(Exercise exercise) {
		this.identifier = exercise.getIdentifier();
		this.name = exercise.getName();
		this.info = exercise.getInfo();
		this.metric = new MetricXML(exercise.getMetric());
	}

	public Object getModel(){
		Exercise exercise = new Exercise();
		exercise.setIdentifier(identifier);
		exercise.setName(name);
		exercise.setInfo(info);
		Metric metric = new Metric();
		metric.setType(this.metric.getType());
		exercise.setMetric(metric);
		return exercise;
	}


	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public MetricXML getMetric() {
		return metric;
	}

	public void setMetric(MetricXML metric) {
		this.metric = metric;
	}
}
