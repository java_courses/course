package com.dp9v.training_timetable.services;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.MetricServiceException;
import com.dp9v.training_timetable.models.converters.MetricConverter;
import com.dp9v.training_timetable.models.entity.ExerciseEntity;
import com.dp9v.training_timetable.models.entity.MetricEntity;
import com.dp9v.training_timetable.models.pojo.Metric;
import com.dp9v.training_timetable.repositories.ExerciseRepository;
import com.dp9v.training_timetable.repositories.MetricRepository;
import com.dp9v.training_timetable.services.interfaces.IMetricService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public class MetricService implements IMetricService {
	private static Logger LOGGER = Logger.getLogger(MetricService.class);

	private MetricRepository   metricRepository;
	private ExerciseRepository exerciseRepository;
	private MetricConverter    converter;

	@Autowired
	public void setExerciseRepository(ExerciseRepository exerciseRepository) {
		this.exerciseRepository = exerciseRepository;
	}

	@Autowired
	public void setConverter(MetricConverter converter) {
		this.converter = converter;
	}

	@Autowired
	public void setMetricRepository(MetricRepository metricRepository) {
		this.metricRepository = metricRepository;
	}

	/**
	 * Получает объект метрики из БД по id
	 *
	 * @param id Первичный ключ метрики в БД
	 * @return объект, представляющий тренировку в базе
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Metric getById(long id) throws MetricServiceException {
		try {
			return converter.mapEntityToPojo(metricRepository.findOne(id));
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new MetricServiceException("Ошибка получения объекта");
		}
	}

	/**
	 * Получает объект метрики из БД по типу
	 *
	 * @param type тип метрики в БД
	 * @return объект, продставляющий тренировку в базе
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Metric getMetricByType(String type) throws MetricServiceException {
		try {
			return converter.mapEntityToPojo(metricRepository.getByType(type));
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new MetricServiceException("Ошибка получения метрики");
		}
	}

	/**
	 * Удаляет из БД метрику с указанным ID
	 *
	 * @param id ID метрики для удаления
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void delete(long id) throws MetricServiceException {
		try {
			List<ExerciseEntity> list = exerciseRepository.findByDeleteTypeAndMetric_Id(0, id);
			if(list.size() > 0)
				throw new MetricServiceException("Нельзя удалить эту метрику, есть связанные с ней упражнения");
			MetricEntity metric = metricRepository.findOne(id);
			metric.setDeleteType(1);
			metricRepository.save(metric);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new MetricServiceException("Ошибка удаления метрики");
		}
	}

	/**
	 * Сохраняет запись в БД
	 *
	 * @param model объет модели для сохранения в БД
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public Metric save(Metric model) throws MetricServiceException {
		MetricEntity entity = null;
		if(model.getId() == 0) {
			entity = converter.mapPojoToEntity(model);
			entity = metricRepository.save(entity);
		} else {
			try {
				entity = metricRepository.findOne(model.getId());
				if(entity.getVersion() > model.getVersion())
					throw new MetricServiceException("Метрика была кем-то отредактирована. Обновите страницу");
				entity.setType(model.getType());
				entity = metricRepository.save(entity);
			}
			catch (RuntimeException e) {
				LOGGER.error(e);
				throw new MetricServiceException("Ошибка сохранения записи");
			}
		}
		return converter.mapEntityToPojo(entity);
	}

	/**
	 * Возвращает список метрик(Metric) в БД
	 *
	 * @return Список метрик в базе
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<Metric> all() throws MetricServiceException {
		try {
			List<MetricEntity> res = (List<MetricEntity>) metricRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
			res = res.stream().filter(metricEntity -> metricEntity.getDeleteType() == 0)
			         .collect(Collectors.toList());
			return converter.entityListToPojoList(res);
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new MetricServiceException("Ошибка получения списка метрик");
		}
	}

	/**
	 * Пакетно загружает в БД список метрик
	 *
	 * @param objects список метрик для загрузки
	 * @throws MetricServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void insertAll(List<Metric> objects) throws MetricServiceException {
		try {
			List<MetricEntity> entities = converter.pojoListToEntityList(objects);
			metricRepository.save(entities);
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new MetricServiceException("Ошибка загрузки метрик");
		}
	}
}
