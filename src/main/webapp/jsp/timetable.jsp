<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--@elvariable id="pastTrainings" type="java.util.ArrayList<com.dp9v.training_timetable.models.pojo.Training>"--%>
<%--@elvariable id="afterTraining" type="java.util.ArrayList<com.dp9v.training_timetable.models.pojo.Training>"--%>

<rapid:override name="title">Список тренировок</rapid:override>

<rapid:override name="content">
    <div class="starter-template">
        <ul class="nav nav-tabs" id="trainingTabs">
            <li class="active"><a href="#futureTab" data-toggle="tab">Запланированные тренировки</a></li>
            <li><a href="#pastTab" data-toggle="tab">Запланированные тренировки</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="futureTab">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Дата</th>
                            <th>Время</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${afterTraining}" var="training">
                            <tr id="row${training.id}">
                                <td class="idValue"><c:out value="${training.id}"/></td>
                                <td class="dateValue"><c:out value="${training.date}"/></td>
                                <td class="beginTimeValue"><c:out value="${training.beginTime}"/></td>
                                <td>
                                    <button class="btn btn-rm" data-id="${training.id}"
                                            data-link="<c:url value="/timetable/delete"/>">Удалить
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-training-edit"
                                            data-version="${training.version}">Редактировать
                                    </button>
                                </td>
                                <td>
                                    <a href="/training/${training.id}" class="btn">Просмотр
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="6">
                                <button class="btn btn-training-create">Добавить</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="pastTab">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Дата</th>
                            <th>Время</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${pastTrainings}" var="training">
                            <tr>
                                <td><c:out value="${training.id}"/></td>
                                <td><c:out value="${training.date}"/></td>
                                <td><c:out value="${training.beginTime}"/></td>
                                <td>
                                    <a class="btn" href="/training/${training.id}">Просомотр</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <jsp:include page="popups/trainingPopup.jsp" flush="true"/>
</rapid:override>


<%@ include file="/jsp/base.jsp" %>