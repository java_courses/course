<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<rapid:override name="title">Авторизация</rapid:override>

<rapid:override name="body">
    <div class="container gray">

        <form id="form-signin" class="form-signin" role="form" method="post" action="${pageContext.request.contextPath}/login">
            <h2 class="form-signin-heading">Авторизируйтесь пожалуйста</h2>
            <input type="email" class="form-control" placeholder="Email address" name="email" required autofocus>
            <input type="password" class="form-control" placeholder="Password" name="password" required>
            <button id="btn-signin"  class="btn btn-lg btn-primary btn-block btn-submit">Авторизация</button>
            <a href="${pageContext.request.contextPath}/login/registration" role="button">Регистрация</a>
        </form>

    </div>
</rapid:override>

<%@ include file="base.jsp" %>