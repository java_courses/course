package com.dp9v.training_timetable.models.entity;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 20.03.17.
 */
@Entity
@Table(name = "training_exercise", schema = "public", catalog = "TrainingTimetable")
public class TrainingExerciseEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "main_seq")
	@SequenceGenerator(name = "main_seq", sequenceName = "main_seq", allocationSize = 1)
	private Long id;

	@Column
	private Double counter;

	@Column
	private String comment;

	@Column(updatable = false, insertable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private UUID identifier;

	@Column(name = "delete_type", insertable = false)
	private Integer deleteType;

	@Version
	@Column
	private Integer version;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "trainingfk")
	private TrainingEntity training;


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "exercisefk")
	private ExerciseEntity exercise;

	public TrainingExerciseEntity() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getCounter() {
		return counter;
	}

	public void setCounter(Double counter) {
		this.counter = counter;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public TrainingEntity getTraining() {
		return training;
	}

	public void setTraining(TrainingEntity training) {
		this.training = training;
	}

	public ExerciseEntity getExercise() {
		return exercise;
	}

	public void setExercise(ExerciseEntity exercise) {
		this.exercise = exercise;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if(!(o instanceof TrainingExerciseEntity))
			return false;

		TrainingExerciseEntity that = (TrainingExerciseEntity) o;

		if(id != null ? !id.equals(that.id) : that.id != null)
			return false;
		if(counter != null ? !counter.equals(that.counter) : that.counter != null)
			return false;
		if(comment != null ? !comment.equals(that.comment) : that.comment != null)
			return false;
		if(identifier != null ? !identifier.equals(that.identifier) : that.identifier != null)
			return false;
		if(deleteType != null ? !deleteType.equals(that.deleteType) : that.deleteType != null)
			return false;
		if(version != null ? !version.equals(that.version) : that.version != null)
			return false;
		if(training != null ? !training.equals(that.training) : that.training != null)
			return false;
		return exercise != null ? exercise.equals(that.exercise) : that.exercise == null;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (counter != null ? counter.hashCode() : 0);
		result = 31 * result + (comment != null ? comment.hashCode() : 0);
		result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
		result = 31 * result + (deleteType != null ? deleteType.hashCode() : 0);
		result = 31 * result + (version != null ? version.hashCode() : 0);
		result = 31 * result + (training != null ? training.hashCode() : 0);
		result = 31 * result + (exercise != null ? exercise.hashCode() : 0);
		return result;
	}
}
