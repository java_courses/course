package com.dp9v.training_timetable.models.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 20.03.17.
 */
@Entity
@Table(name = "exercises", schema = "public", catalog = "TrainingTimetable")
public class ExerciseEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "main_seq")
	@SequenceGenerator(name = "main_seq", sequenceName = "main_seq", allocationSize = 1)
	private Long id;

	@Column(length = 50)
	private String name;

	@Column(length = 500)
	private String info;

	@Column(updatable = false, insertable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private UUID identifier;


	@Column(name = "delete_type", insertable = false)
	private Integer deleteType;

	@Column
	@Version
	private Integer version;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "metricfk")
	private MetricEntity metric;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "exercise")
	private Set<TrainingExerciseEntity> trainingExercises = new HashSet<>();

	public ExerciseEntity() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public MetricEntity getMetric() {
		return metric;
	}

	public void setMetric(MetricEntity metric) {
		this.metric = metric;
	}
}
