package com.dp9v.training_timetable.common.utils;

/**
 * Created by dpolkovnikov on 15.02.17.
 */

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class Mailer {

	private static Logger logger   = Logger.getLogger(Mailer.class);
	private static String EMAIL    = "no_reply_test_mail@mail.ru";
	private static String PASSWORD = "D11235815";

	public static void sendMessage(String subject, String message, String mailTo) {
		Properties props = new Properties();
		String PORT = "465";
		String HOST = "smtp.mail.ru";

		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", PORT);
		props.put("mail.smtp.socketFactory.class",
		          "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", HOST);
		props.put("mail.smtp.port", PORT);


		Session session = Session.getDefaultInstance(props,
		                                             new javax.mail.Authenticator() {
			                                             protected PasswordAuthentication getPasswordAuthentication() {
				                                             return new PasswordAuthentication(EMAIL, PASSWORD);
			                                             }
		                                             });
		try {

			Message message_ = new MimeMessage(session);
			message_.setFrom(new InternetAddress(EMAIL));
			message_.setRecipients(Message.RecipientType.TO,
			                       InternetAddress.parse(mailTo));
			message_.setSubject(subject);
			message_.setText(message);

			Transport.send(message_);
		}
		catch (MessagingException e) {
			logger.fatal(e);
			throw new RuntimeException(e);
		}
	}
}