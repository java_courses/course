package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.common.utils.Mailer;
import com.dp9v.training_timetable.forms.RegistrationForm;
import com.dp9v.training_timetable.models.pojo.User;
import com.dp9v.training_timetable.services.UserService;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 04.03.17.
 */
@Controller("auth")
@RequestMapping(value = "/login")
public class AuthController {
	private IUserService userService;

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration() {
		return "registration";
	}


	@RequestMapping(value = "/confirmEmail", method = RequestMethod.GET)
	public String confirmEmail(@RequestParam("token") UUID token, Model model) {
		try {
			userService.activateUser(token);
			model.addAttribute("title", "Успех");
			model.addAttribute("message", "e-mail подтвержден");
			model.addAttribute("href", "/login");
			model.addAttribute("hrefMessage", "Перейти на страницу авторизации");
		}
		catch (UserServiceException e) {
			model.addAttribute("title", "Ошибка");
			model.addAttribute("message", e.getMessage());
			model.addAttribute("href", "/");
			model.addAttribute("hrefMessage", "Перейти на главную");
		}
		return "message";
	}

	@ResponseBody
	@RequestMapping(value = "/loginResult", method = RequestMethod.GET)
	public Map<String, Object> loginResult(Principal principal) {
		Map<String, Object> res = new HashMap<>(4);
		if(principal != null) {
			res.put("success", true);
			res.put("message", "/");
		} else {
			res.put("success", false);
			res.put("message", "Ошибка авторизации пользователя");
		}
		return res;
	}


	@ResponseBody
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public Map<String, Object> registration(@Valid RegistrationForm form,
	                                        BindingResult bindingResult, HttpServletRequest req) {
		Map<String, Object> res = new HashMap<>();
		if(bindingResult.hasErrors()) {
			res.put("success", false);
			res.put("message", bindingResult.getFieldError().getDefaultMessage());
			return res;
		}
		User user = form.getUser();
		try {
			user = userService.registerUser(user);
			String url = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + "/";
			res.put("success", true);
			res.put("message",
			        "Регистрация прошла успешно.\nНа указанный e-mail отправленно письмо с подтверждением");
			Mailer.sendMessage("Подтверждение пароля",
			                   "Вы зарегистрировались на сайте. Для подтверждения пароля перейдите по ссылке: " +
			                   url + "login/confirmEmail?token=" + user.getToken(), user.getEmail());
		}
		catch (UserServiceException e) {
			res.put("success", false);
			res.put("message", e.getMessage());
		}
		return res;

	}
}

