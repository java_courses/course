<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" id="exerciseModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="exerciseFormTitle">Редактирование упражнения</h4>
            </div>
            <form id="exerciseForm" method="post" action="${pageContext.request.contextPath}/admin/saveExercise">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="version" id="version" value="0">
                    <label for="name">Название упражнения</label>
                    <input type="text" class="form-control" name="name" id="name" value="">
                    <label for="info">Описание упражнения</label>
                    <textarea class="form-control" name="info" id="info" rows="3"></textarea>
                    <label for="metricfk">Метрика</label>
                    <select class="form-control" name="metricfk" id="metricfk"></select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-form-submit-exercise">Сохранить изменения</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
</div>