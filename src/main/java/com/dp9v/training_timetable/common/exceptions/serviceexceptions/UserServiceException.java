package com.dp9v.training_timetable.common.exceptions.serviceexceptions;

/**
 * Created by dpolkovnikov on 07.03.17.
 */
public class UserServiceException extends ServiceException {
	public UserServiceException() {
		super();
	}

	public UserServiceException(String message) {
		super(message);
	}

	public UserServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public UserServiceException(Throwable cause) {
		super(cause);
	}

	protected UserServiceException(String message, Throwable cause, boolean enableSuppression,
	                               boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
