package com.dp9v.training_timetable.common.loader.lists;


import com.dp9v.training_timetable.common.loader.models.ExerciseXML;
import com.dp9v.training_timetable.models.pojo.Exercise;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlRootElement(name = "Exercise")
@XmlType(propOrder = {"items"})
@XmlSeeAlso(ExerciseXML.class)
public class Exercises extends AbstractModelList<ExerciseXML> {
	public Exercises(ArrayList<ExerciseXML> items) {
		super(items);
	}

	public void fillExercises(List<Exercise> items)
	{
		for(Exercise item: items) {
			this.putItem(new ExerciseXML(item));
		}
	}

	public Exercises() {
	}

	public ArrayList<Exercise> getModels(int begin, int count){
		ArrayList<Exercise> exercises = new ArrayList<>(count);
		for(int i=begin; i<begin+count && i<this.getItems().size(); i++){
			exercises.add((Exercise) getItems().get(i).getModel());
		}
		return exercises;
	}
}
