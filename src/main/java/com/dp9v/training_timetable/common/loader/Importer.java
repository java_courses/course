package com.dp9v.training_timetable.common.loader;

import com.dp9v.training_timetable.common.loader.lists.*;
import com.dp9v.training_timetable.common.marshaller.XMLMarshaller;
import com.dp9v.training_timetable.repositories.*;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

/**
 * Класс реализующий многопоточную загрузку объектов в ORM
 * <p>
 * Created by dpolkovnikov on 25.02.17.
 */
public class Importer implements Callable<AbstractModelList> {
	private static          Logger  logger   = Logger.getLogger(Importer.class);
	private static volatile boolean stopFlag = false;
	private String filePath;
	private String tableName;

	/**
	 * Конструктор
	 *
	 * @param filePath Путь к xml-файлу. Название файла = название таблицы
	 */
	public Importer(String filePath) {
		this.filePath = filePath;
		tableName = Paths.get(filePath).getFileName().toString().replaceAll(".xml", "");
	}

	/**
	 * Запуск потока, возвращающего объектное представление таблицы из xml-файла
	 *
	 * @return Список объектов, загруженных из файла
	 */
	@Override
	public AbstractModelList call() {
		AbstractModelList list = null;
		try {
			switch(tableName) {
				case UserRepository.TABLE_NAME:
					list = (Users) XMLMarshaller.getObject(new File(filePath), Users.class);
					break;
				case TrainingExerciseRepository.TABLE_NAME:
					list = (TrainingExercises) XMLMarshaller.getObject(new File(filePath), TrainingExercises.class);
					break;
				case TrainingRepository.TABLE_NAME:
					list = (Trainings) XMLMarshaller.getObject(new File(filePath), Trainings.class);
					break;
				case MetricRepository.TABLE_NAME:
					list = (Metrics) XMLMarshaller.getObject(new File(filePath), Metrics.class);
					break;
				case ExerciseRepository.TABLE_NAME:
					list = (Exercises) XMLMarshaller.getObject(new File(filePath), Exercises.class);
					break;
			}
		}
		catch (JAXBException e) {
			e.printStackTrace();
			stopFlag = true;
		}
		if(!stopFlag)
			return list;
		else
			return null;
	}
}
