package com.dp9v.training_timetable.common.loader;

import com.dp9v.training_timetable.common.loader.lists.AbstractModelList;
import com.dp9v.training_timetable.services.interfaces.ServiceInterface;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Класс, содержаший статические методы для загрузки и выгрузки данных из БД
 * <p>
 * Created by dpolkovnikov on 25.02.17.
 */
public class Executor {
	private static Logger                logger      = Logger.getLogger(Executor.class);
	private ArrayList<AbstractModelList> lists       = new ArrayList<>();
	private int                          beforeCount = 0;

	/**
	 * Запуск выгрузки данных из БД в XML
	 *
	 * @param tables  массив названий таблиц для выгрузки
	 * @param dirPath директория в которую сохраняются файлы
	 */
	public void runExporters(String[] tables, String dirPath, ServiceInterface[] services) {
		ExecutorService pool = Executors.newFixedThreadPool(10);
		for(int i=0; i<services.length;i++){
			Thread th = new Thread(new Exporter(dirPath, tables[i], services[i]));
			pool.submit(th);
		}
		pool.shutdown();
		try {
			pool.awaitTermination(5, TimeUnit.MINUTES);
		}
		catch (InterruptedException e) {
			logger.error(logger);
		}
	}

	/**
	 * Запуск загрузки данных из XML в ORM
	 *
	 * @param files массив названий файлов для загрузки в ORM
	 */
	public void runImporters(String[] files) {
		ThreadPoolExecutor              executor   = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
		List<Future<AbstractModelList>> resultList = new ArrayList<>();
		for(String file : files) {
			Importer                  importer = new Importer(file);
			Future<AbstractModelList> result   = executor.submit(importer);
			resultList.add(result);
		}

		for(Future<AbstractModelList> result : resultList) {
			try {
				AbstractModelList loadList = result.get();
				beforeCount += loadList.getItems().size();
				lists.add(loadList);
			}
			catch (InterruptedException | ExecutionException e) {
				logger.error(e);
			}
		}
		executor.shutdown();
	}

	/**
	 * Старт загрузки объетов из списка в БД
	 */
	private void runLoad(ServiceInterface[] services) {
		ThreadPoolExecutor    executor   = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
		int                   afterCount = beforeCount;
		List<Future<Integer>> resultList = new ArrayList<>();
		do {
			beforeCount = afterCount;
			afterCount = 0;
			for(int i=0; i<services.length;i++){
				Loader          loader = new Loader(lists.get(i), services[i]);
				Future<Integer> result = executor.submit(loader);
				resultList.add(result);
			}
			for(Future<Integer> result : resultList) {
				try {
					int temp = result.get();
					System.out.println(temp);
					afterCount += temp;
				}
				catch (InterruptedException | ExecutionException e) {
					logger.error(e);
				}
			}
			resultList.clear();
		}
		while(afterCount != 0 && beforeCount != afterCount);
		executor.shutdown();
	}
}
