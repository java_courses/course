package com.dp9v.training_timetable.common.exceptions.serviceexceptions;

/**
 * Created by dpolkovnikov on 13.03.17.
 */
public class TrainingServiceException extends ServiceException {
	public TrainingServiceException() {
	}

	public TrainingServiceException(String message) {
		super(message);
	}

	public TrainingServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public TrainingServiceException(Throwable cause) {
		super(cause);
	}

	public TrainingServiceException(String message, Throwable cause, boolean enableSuppression,
	                                boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
