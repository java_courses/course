package com.dp9v.training_timetable.services.auth;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.models.converters.UserConverter;
import com.dp9v.training_timetable.models.pojo.User;
import com.dp9v.training_timetable.repositories.UserRepository;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by dpolkovnikov on 13.03.17.
 */
@Service
public class AuthUserService implements UserDetailsService {
	private Logger logger = Logger.getLogger(AuthUserService.class);

	private UserRepository repository;
	private UserConverter converter;

	@Autowired
	public void setConverter(UserConverter converter) {
		this.converter = converter;
	}

	@Autowired
	public void setRepository(UserRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			User user = converter.mapEntityToPojo(repository.getByEmail(username.toLowerCase()));
			return new UserDetailsImpl(user);
		}
		catch (RuntimeException e) {
			logger.error(e);
			throw new UsernameNotFoundException(username + " not found");
		}
	}


	public class UserDetailsImpl implements UserDetails {

		User user;

		public UserDetailsImpl(User user) {
			this.user = user;
		}

		public User getUser() {
			return user;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			ArrayList<SimpleGrantedAuthority> res = new ArrayList<>(2);
			res.add(new SimpleGrantedAuthority(user.getIsAdmin() ? "ROLE_ADMIN" : "ROLE_USER"));
			return res;
		}

		@Override
		public String getPassword() {
			return user.getPassword();
		}

		@Override
		public String getUsername() {
			return user.getEmail();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return user.getIsActive();
		}
	}
}
