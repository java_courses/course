package com.dp9v.training_timetable.services;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingExerciseServiceException;
import com.dp9v.training_timetable.models.converters.TrainingExerciseConverter;
import com.dp9v.training_timetable.models.entity.TrainingExerciseEntity;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;
import com.dp9v.training_timetable.repositories.ExerciseRepository;
import com.dp9v.training_timetable.repositories.TrainingExerciseRepository;
import com.dp9v.training_timetable.repositories.TrainingRepository;
import com.dp9v.training_timetable.services.interfaces.ITrainingExerciseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dpolkovnikov on 25.02.17.
 */
@Service
public class TrainingExerciseService implements ITrainingExerciseService {
	private static Logger LOGGER = Logger.getLogger(TrainingExerciseService.class);
	private TrainingExerciseRepository trainingExerciseRepository;
	private TrainingRepository         trainingRepository;
	private ExerciseRepository         exerciseRepository;
	private TrainingExerciseConverter  converter;

	@Autowired
	public void setExerciseRepository(ExerciseRepository exerciseRepository) {
		this.exerciseRepository = exerciseRepository;
	}

	@Autowired
	public void setTrainingRepository(TrainingRepository trainingRepository) {
		this.trainingRepository = trainingRepository;
	}

	@Autowired
	public void setTrainingExerciseConverter(
			TrainingExerciseConverter trainingExerciseConverter) {
		this.converter = trainingExerciseConverter;
	}

	@Autowired
	public void setTrainingExerciseRepository(
			TrainingExerciseRepository trainingExerciseRepository) {
		this.trainingExerciseRepository = trainingExerciseRepository;
	}


	/**
	 * Возвращает список промежуточных таблиц(training_exercise) в БД
	 *
	 * @return Список промежуточных таблиц в базе
	 * @throws TrainingExerciseServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<TrainingExercise> all() throws TrainingExerciseServiceException {
		try {
			List<TrainingExerciseEntity> res = trainingExerciseRepository.findByDeleteTypeOrderByIdAsc(0);
			return converter.entityListToPojoList(res);
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new TrainingExerciseServiceException("Ошибка подключения к базе");
		}
	}


	/**
	 * Загрузка списка записей в БД
	 *
	 * @param objects список pojo-записей для загрузки
	 * @throws TrainingExerciseServiceException ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void insertAll(List<TrainingExercise> objects) throws TrainingExerciseServiceException {
		try {
			trainingExerciseRepository.save(converter.pojoListToEntityList(objects));
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new TrainingExerciseServiceException("Ошибка подключения к базе");
		}
	}

	/**
	 * Получение pojo-представление объекта в БД
	 *
	 * @param id ID объекта из БД
	 * @return pojo-представление записи в БД
	 * @throws TrainingExerciseServiceException Ошибка работы сервиса
	 */
	@Override
	public TrainingExercise getById(long id) throws TrainingExerciseServiceException {
		try {
			TrainingExerciseEntity te = trainingExerciseRepository.findOne(id);
			return converter.mapEntityToPojo(te);
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new TrainingExerciseServiceException("Ошибка подключения к базе");
		}
	}

	/**
	 * Удалени записи из БД
	 *
	 * @param id ID записи для удаления
	 * @throws TrainingExerciseServiceException ошибка работы сервиса
	 */
	@Override
	public void delete(long id) throws TrainingExerciseServiceException {
		try {
			TrainingExerciseEntity entity = trainingExerciseRepository.findOne(id);
			entity.setDeleteType(1);
			trainingExerciseRepository.save(entity);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new TrainingExerciseServiceException("Ошибка удаления записи");
		}
	}

	/**
	 * Сохранение записи в БД
	 *
	 * @param model pojo-объект для сохранения в БД
	 * @param name  имя пользователя, для проверки прав при сохранении
	 * @return pojo-экземпляр объекта, после сохранения в БД
	 * @throws TrainingExerciseServiceException ошибка работы сервиса
	 */
	@Override
	public TrainingExercise save(TrainingExercise model, String name) throws TrainingExerciseServiceException {
		try {
			TrainingExerciseEntity entity = converter.mapPojoToEntity(model);
			entity.setTraining(trainingRepository.findOne(model.getTraining().getId()));
			entity.setExercise(exerciseRepository.findOne(model.getExercise().getId()));
			if(!entity.getTraining().getUser().getEmail().equalsIgnoreCase(name)) {
				throw new TrainingExerciseServiceException("У вас не прав для редактирования");
			}
			if(entity.getId() == 0) {
				entity = trainingExerciseRepository.save(entity);
			} else {
				TrainingExerciseEntity newEntity = trainingExerciseRepository.findOne(entity.getId());
				if(newEntity.getVersion() > entity.getVersion())
					throw new TrainingExerciseServiceException("Запись была кем-то отредактирована, обновите страницу");
				newEntity.setCounter(entity.getCounter());
				newEntity.setComment(entity.getComment());
				newEntity.setExercise(entity.getExercise());
				entity = trainingExerciseRepository.save(newEntity);
			}
			return converter.mapEntityToPojo(entity);
		}
		catch (RuntimeException ex) {
			LOGGER.error("", ex);
			throw new TrainingExerciseServiceException("Ошибка сохранения записи");
		}
	}

	@Override
	public Boolean checkAccess(Long id, String email) throws TrainingExerciseServiceException {
		try {
			TrainingExerciseEntity entity = trainingExerciseRepository.findOne(id);
			return entity.getTraining().getUser().getEmail().equalsIgnoreCase(email);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new TrainingExerciseServiceException("Ошибка проверки доступа");
		}
	}

	@Override
	public List<TrainingExercise> findForTraining(Long id) throws TrainingExerciseServiceException {
		try {
			return converter.entityListToPojoList(trainingExerciseRepository.findByDeleteTypeAndTraining_Id(0, id));
		}
		catch (RuntimeException ex) {
			throw new TrainingExerciseServiceException("Ошибка получения списка упражнений");
		}
	}
}
