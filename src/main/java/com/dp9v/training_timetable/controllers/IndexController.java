package com.dp9v.training_timetable.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dpolkovnikov on 05.03.17.
 */
@Controller("index")
public class IndexController {

	@RequestMapping(value = {"", "/index"}, method = RequestMethod.GET)
	public String index(HttpServletRequest request) {
		if(request.isUserInRole("ROLE_ADMIN"))
			return "index";
		else
			return "redirect:timetable";
	}
}
