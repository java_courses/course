package com.dp9v.training_timetable.common.loader.lists;

import com.dp9v.training_timetable.common.loader.models.MetricXML;
import com.dp9v.training_timetable.models.pojo.Metric;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlRootElement(name = "Metric")
@XmlType(propOrder = {"items"})
@XmlSeeAlso(MetricXML.class)
public class Metrics extends AbstractModelList<MetricXML> {
	public Metrics(ArrayList<MetricXML> items) {
		super(items);
	}

	public void fillMetrics(List<Metric> items)
	{
		for(Metric item: items) {
			this.putItem(new MetricXML(item));
		}
	}

	public Metrics() {
	}

	public ArrayList<Metric> getModels(int begin, int count){
		ArrayList<Metric> metric = new ArrayList<>(count);
		for(int i=begin; i<begin+count && i<this.getItems().size(); i++){
			metric.add((Metric) getItems().get(i).getModel());
		}
		return metric;
	}
}
