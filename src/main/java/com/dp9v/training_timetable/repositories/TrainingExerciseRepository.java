package com.dp9v.training_timetable.repositories;

import com.dp9v.training_timetable.models.entity.TrainingExerciseEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public interface TrainingExerciseRepository extends PagingAndSortingRepository<TrainingExerciseEntity, Long> {
	public final String TABLE_NAME = "training_exercise";

	TrainingExerciseEntity getByIdentifier(UUID identifier);

	List<TrainingExerciseEntity> findByDeleteTypeAndExercise_Id(Integer deleteType, Long id);

	List<TrainingExerciseEntity> findByDeleteTypeAndTraining_Id(Integer deleteType, Long id);

	List<TrainingExerciseEntity> findByDeleteTypeOrderByIdAsc(Integer deleteType);
}
