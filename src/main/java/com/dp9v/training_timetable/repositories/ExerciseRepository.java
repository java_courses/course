package com.dp9v.training_timetable.repositories;

import com.dp9v.training_timetable.models.entity.ExerciseEntity;
import com.dp9v.training_timetable.models.entity.TrainingEntity;
import com.dp9v.training_timetable.models.entity.TrainingExerciseEntity;
import com.dp9v.training_timetable.models.pojo.Metric;
import com.dp9v.training_timetable.services.TrainingExerciseService;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public interface ExerciseRepository extends PagingAndSortingRepository<ExerciseEntity, Long> {
	public final String TABLE_NAME = "exercises";

	ExerciseEntity getByIdentifier(UUID identifier);
	List<ExerciseEntity> findByDeleteTypeAndMetric_Id(Integer deleteType, Long id);
}
