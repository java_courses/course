<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" id="metricModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="metricFormTitle">Редактирование метрики</h4>
            </div>
            <form id="metricForm" method="post" action="${pageContext.request.contextPath}/admin/saveMetric">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="version" id="version" value="0">
                    <label for="type">Название метрики</label>
                    <input type="text" class="form-control" name="type" id="type" placeholder="Название метрики" value="">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-form-submit-metric">Сохранить изменения</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
</div>