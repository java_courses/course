<%--
  Created by IntelliJ IDEA.
  User: dpolkovnikov
  Date: 27.02.17
  Time: 0:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="/static/css/style.css">
    <title><rapid:block name="title">Base title</rapid:block></title>
</head>
<body>
<rapid:block name="body">
    <rapid:block name="header">
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">Расписание тренировок</a>
                </div>
                <div class="navbar-right">

                    <a href="/logout">Выйти</a>
                </div>
            </div>
        </div>
    </rapid:block>

    <div class="container">

        <rapid:block name="content">
            <div class="starter-template">
            </div>
        </rapid:block>


        <rapid:block name="footer">
            <hr>
            <footer>
                <p>Расписание тренировок</p>
            </footer>
        </rapid:block>
    </div>
</rapid:block>
<jsp:include page="popups/message.jsp" flush="true"/>

<script src="http://code.jquery.com/jquery-3.1.1.js"
        integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="/static/js/main.js"></script>
</body>
</html>
