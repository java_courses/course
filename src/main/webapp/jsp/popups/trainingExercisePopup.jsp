<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="allExercises" type="java.util.List<com.dp9v.training_timetable.models.pojo.Exercise>"--%>
<%--@elvariable id="training" type="java.lang.Integer"--%>

<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" id="trainingExerciseModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="trainingExerciseFormTitle">Редактирование упражнения</h4>
            </div>

            <form id="trainingExerciseForm" method="post" action="/training/insert">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="version" id="version" value="0">
                    <input type="hidden" name="trainingfk" id="trainingfk" value="${training}">
                    <label for="exercise">Упражнение</label>
                    <select class="form-control" name="exercisefk" id="exercise">
                        <c:forEach items="${allExercises}" var="exercise">
                            <option value="${exercise.id}">${exercise}</option>
                        </c:forEach>
                    </select>
                    <label for="counter">Результат выполнения</label>
                    <input type="number" id="counter" name="counter" value="" class="form-control">
                    <label for="comment">Комментарий</label>
                    <input type="text" id="comment" name="comment" value="" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-form-submit-trainingExercise">Сохранить изменения
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
</div>