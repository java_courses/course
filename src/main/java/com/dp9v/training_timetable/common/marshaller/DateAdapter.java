package com.dp9v.training_timetable.common.marshaller;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Date;

/**
 * xmlAdapter для преобразования java.sql.Date к String при маршализациии и обратно
 * <p>
 * Created by dpolkovnikov on 21.02.17.
 */
public class DateAdapter extends XmlAdapter<String, Date> {
	/**
	 * Преобразует строку v к типу java.sql.Date
	 *
	 * @param v - Входная строка фомата yyyy-mm-dd
	 * @return Значение типа java.sql.Date, соответсвующее входной строке
	 */
	@Override
	public Date unmarshal(String v) {
		return Date.valueOf(v);
	}

	/**
	 * Преобразует дату v к Строке
	 *
	 * @param v объект java.sql.Date
	 * @return Строчное представление входного объекта
	 */
	@Override
	public String marshal(Date v) {
		return v.toString();
	}
}
