package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.models.pojo.Metric;
import com.dp9v.training_timetable.models.pojo.User;
import com.dp9v.training_timetable.services.interfaces.IExerciseService;
import com.dp9v.training_timetable.services.interfaces.IMetricService;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dpolkovnikov on 04.03.17.
 */
@Controller("admin")
@RequestMapping(value = "/admin")
public class AdminController {

	private IUserService     userService;
	private IExerciseService exerciseService;
	private IMetricService   metricService;

	@Autowired
	public void setMetricService(IMetricService metricService) {
		this.metricService = metricService;
	}

	@Autowired
	public void setExerciseService(IExerciseService exerciseService) {
		this.exerciseService = exerciseService;
	}

	@Autowired
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String adminPanel(Model model, Principal principal) {
		try {
			Long           userId    = userService.getByEmail(principal.getName()).getId();
			List<User>     users     = userService.all();
			List<Exercise> exercises = exerciseService.all();
			List<Metric>   metrics   = metricService.all();
			model.addAttribute("users", users);
			model.addAttribute("metrics", metrics);
			model.addAttribute("exercises", exercises);
			if(userId != null) {
				User user = userService.getById(userId);
				model.addAttribute("currentUser", user);
			}
			return "admin";
		}
		catch (ServiceException e) {
			model.addAttribute("title", "Ошибка");
			model.addAttribute("message", e.getMessage());
			model.addAttribute("href", "/");
			model.addAttribute("hrefMessage", "Перейти на главную");
			return "message";
		}
	}

	@ResponseBody
	@RequestMapping(value = "/setUserValue", method = RequestMethod.POST)
	public Map<String, Object> login(@RequestParam("id") Long id,
	                                 @RequestParam("name") String name,
	                                 @RequestParam("value") Boolean value) {
		Map<String, Object> res = new HashMap<>(2);
		try {
			userService.setValue(id, name, value);
			res.put("success", true);
		}
		catch (UserServiceException e) {
			res.put("success", false);
		}
		return res;
	}
}
