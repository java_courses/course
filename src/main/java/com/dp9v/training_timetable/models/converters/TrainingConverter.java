package com.dp9v.training_timetable.models.converters;

import com.dp9v.training_timetable.models.entity.TrainingEntity;
import com.dp9v.training_timetable.models.pojo.Training;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Service
public class TrainingConverter {
	private final static MapperFacade mapper;

	static {
		final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(Training.class, TrainingEntity.class)
		             .byDefault().register();
		mapper = mapperFactory.getMapperFacade();
	}

	public TrainingConverter() {
	}

	public Training mapEntityToPojo(final TrainingEntity training) {
		Training pDto = mapper.map(training, Training.class);
		return pDto;
	}

	public TrainingEntity mapPojoToEntity(final Training training) {
		TrainingEntity pDto = mapper.map(training, TrainingEntity.class);
		return pDto;
	}

	public List<TrainingEntity> pojoListToEntityList(List<Training> trainings) {
		ArrayList<TrainingEntity> res = new ArrayList<>();
		for(Training training : trainings) {
			res.add(mapPojoToEntity(training));
		}
		return res;
	}

	public List<Training> entityListToPojoList(List<TrainingEntity> trainings) {
		ArrayList<Training> res = new ArrayList<>();
		for(TrainingEntity training : trainings) {
			res.add(mapEntityToPojo(training));
		}
		return res;
	}
}
