<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" id="trainingModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="trainingFormTitle">Редактирование тренировки</h4>
            </div>
            <form id="trainingForm" method="post" action="${pageContext.request.contextPath}/timetable/insert">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id" value="0">
                    <input type="hidden" name="version" id="version" value="0">
                    <label for="date">Дата тренировки</label>
                    <input type="date" id="date" name="date" value="" class="form-control">
                    <label for="beginTime">Время тренировки</label>
                    <input type="time" id="beginTime" name="beginTime" value="" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-form-submit-training">Сохранить изменения</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div>
</div>