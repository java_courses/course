package com.dp9v.training_timetable.forms;

import com.dp9v.training_timetable.models.pojo.Metric;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by dpolkovnikov on 15.03.17.
 */
public class MetricForm {


	private Long id;

	@NotNull
	@Size(min = 0, message = "Значение типа слишком короткое")
	private String type;

	@NotNull(message = "Версия не указана")
	@Min(value = 0, message = "Неправильная версия")
	private Integer version;

	public MetricForm() {
		this.id = 0L;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Metric getModel() {
		Metric metric = new Metric(this.getId(), this.getType());
		metric.setVersion(version);
		return metric;
	}
}

