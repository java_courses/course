package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ExerciseServiceException;
import com.dp9v.training_timetable.models.pojo.Exercise;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public interface IExerciseService extends ServiceInterface<Exercise> {
	public long getIdByIdentifier(UUID identifier) throws ExerciseServiceException;

	public UUID getIdentifierBuId(long id) throws ExerciseServiceException;

	public Exercise getByIdentifier(UUID identifier) throws ExerciseServiceException;

	Exercise save(Exercise exercise) throws ExerciseServiceException;
}
