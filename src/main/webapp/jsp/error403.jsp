<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<rapid:override name="title">Авторизация</rapid:override>

<rapid:override name="body">
    <div class="container gray">

        <div class="jumbotron">
            <h1>Ошибка</h1>
            <p class="lead">К сожалению эта страница недоступна</p>
            <p><a class="btn btn-lg btn-success" href="/" role="button">Перейти на главную</a></p>
        </div>

    </div>
</rapid:override>

<%@ include file="base.jsp" %>