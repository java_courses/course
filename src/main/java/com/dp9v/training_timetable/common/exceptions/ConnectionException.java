package com.dp9v.training_timetable.common.exceptions;

/**
 * Created by dpolkovnikov on 23.02.17.
 */
public class ConnectionException extends Exception {
	public ConnectionException() {
		super();
	}

	public ConnectionException(String message) {
		super(message);
	}

	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionException(Throwable cause) {
		super(cause);
	}

	protected ConnectionException(String message, Throwable cause, boolean enableSuppression,
	                              boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
