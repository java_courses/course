package com.dp9v.training_timetable.repositories;

import com.dp9v.training_timetable.models.entity.MetricEntity;
import com.dp9v.training_timetable.models.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	public final String TABLE_NAME = "users";

	UserEntity getByIdentifier(UUID identifier);
	UserEntity getByEmail(String email);
	UserEntity getByToken(UUID token);
}
