package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingServiceException;
import com.dp9v.training_timetable.models.pojo.Training;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public interface ITrainingService extends ServiceInterface<Training> {

	public Training getByIdentifier(UUID identifier) throws TrainingServiceException;

	public long getIdByIdentifier(UUID identifier) throws TrainingServiceException;

	Training save(Training model) throws TrainingServiceException;

	List<Training> allByUserId(Long id) throws TrainingServiceException;

	Boolean checkAccess(Long id, String email) throws TrainingServiceException;
}
