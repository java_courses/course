package com.dp9v.training_timetable.models.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Entity(name = "Users")
@Table(name = "users")
public class UserEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "main_seq")
	@SequenceGenerator(name = "main_seq", sequenceName = "main_seq", allocationSize = 1)
	private Long id;

	@Column(length = 250)
	private String name;

	@Column(length = 250, unique = true)
	private String email;

	@Column
	private String password;

	@Column
	private Date birthday;

	@Column(name = "is_admin", insertable = false)
	private Boolean isAdmin;

	@Column(name = "is_active", insertable = false)
	private Boolean isActive;

	@Column(name = "get_notification", insertable = false)
	private Boolean getNotifications;

	@Column(updatable = false, insertable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private UUID token;

	@Column(updatable = false, insertable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private UUID identifier;

	@Column(name = "delete_type", insertable = false)
	private Integer deleteType;

	@Column
	@Version
	private Integer version;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<TrainingEntity> trainings = new TreeSet<>();

	public UserEntity() {
	}

	public Set<TrainingEntity> getTrainings() {
		return trainings;
	}

	public void setTrainings(Set<TrainingEntity> trainings) {
		this.trainings = trainings;
	}

	public Boolean getAdmin() {
		return isAdmin;
	}

	public void setAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public Boolean getActive() {
		return isActive;
	}

	public void setActive(Boolean active) {
		isActive = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean active) {
		isActive = active;
	}

	public Boolean getGetNotifications() {
		return getNotifications;
	}

	public void setGetNotifications(Boolean getNotifications) {
		this.getNotifications = getNotifications;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
