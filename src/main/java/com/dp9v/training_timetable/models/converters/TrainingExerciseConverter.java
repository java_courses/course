package com.dp9v.training_timetable.models.converters;

import com.dp9v.training_timetable.models.entity.TrainingExerciseEntity;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Service
public class TrainingExerciseConverter {
	private final static MapperFacade mapper;

	static {
		final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(TrainingExercise.class, TrainingExerciseEntity.class)
		             .byDefault().register();
		mapper = mapperFactory.getMapperFacade();
	}

	public TrainingExerciseConverter() {
	}

	public TrainingExercise mapEntityToPojo(final TrainingExerciseEntity trainingExercise) {
		TrainingExercise pDto = mapper.map(trainingExercise, TrainingExercise.class);
		return pDto;
	}

	public TrainingExerciseEntity mapPojoToEntity(final TrainingExercise trainingExercise) {
		TrainingExerciseEntity pDto = mapper.map(trainingExercise, TrainingExerciseEntity.class);
		return pDto;
	}

	public List<TrainingExerciseEntity> pojoListToEntityList(List<TrainingExercise> trainingExercises) {
		ArrayList<TrainingExerciseEntity> res = new ArrayList<>();
		for(TrainingExercise trainingExercise : trainingExercises) {
			res.add(mapPojoToEntity(trainingExercise));
		}
		return res;
	}

	public List<TrainingExercise> entityListToPojoList(List<TrainingExerciseEntity> trainingExercises) {
		ArrayList<TrainingExercise> res = new ArrayList<>();
		for(TrainingExerciseEntity trainingExercise : trainingExercises) {
			res.add(mapEntityToPojo(trainingExercise));
		}
		return res;
	}
}
