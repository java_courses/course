package com.dp9v.training_timetable.common.marshaller;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Вспомогательный класс для маршализации объектов в Xml и обратно
 *
 * Created by dpolkovnikov on 25.02.17.
 */
public class XMLMarshaller {

	/**
	 * Демаршализирует xml из из файла в объект
	 *
	 * @param file файл, содержащий xml
	 * @param c Класс, объект которого необходимо демаршализировать
	 * @return Объект класса @c состояние которого описанно в xml
	 * @throws JAXBException ошибка демаршализации
	 */
	public static Object getObject(File file, Class c) throws JAXBException {
		JAXBContext  context      = JAXBContext.newInstance(c);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		return unmarshaller.unmarshal(file);
	}

	/**
	 * Маршализирует объект и сохраняет его в xml-файл
	 *
	 * @param file файл в который необходимо сохранить объект
	 * @param o исходный объект для маршализации
	 * @throws JAXBException ошибка маршализации
	 */
	public static void saveObject(File file, Object o) throws JAXBException {
		JAXBContext context    = JAXBContext.newInstance(o.getClass());
		Marshaller  marshaller = context.createMarshaller();
		marshaller.marshal(o, file);
	}
}
