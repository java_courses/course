package com.dp9v.training_timetable.models.converters;

import com.dp9v.training_timetable.models.entity.UserEntity;
import com.dp9v.training_timetable.models.pojo.User;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Service
public class UserConverter {
	private final static MapperFacade mapper;

	static {
		final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(User.class, UserEntity.class)
		             .byDefault().register();
		mapper = mapperFactory.getMapperFacade();
	}

	public UserConverter() {
	}

	public User mapEntityToPojo(final UserEntity user) {
		User pDto = mapper.map(user, User.class);
		return pDto;
	}

	public UserEntity mapPojoToEntity(final User user) {
		UserEntity pDto = mapper.map(user, UserEntity.class);
		return pDto;
	}

	public List<UserEntity> pojoListToEntityList(List<User> users) {
		ArrayList<UserEntity> res = new ArrayList<>();
		for(User user : users) {
			res.add(mapPojoToEntity(user));
		}
		return res;
	}

	public List<User> entityListToPojoList(List<UserEntity> users) {
		ArrayList<User> res = new ArrayList<>();
		for(UserEntity user : users) {
			res.add(mapEntityToPojo(user));
		}
		return res;
	}
}
