package com.dp9v.training_timetable.forms;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.MetricServiceException;
import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.models.pojo.Metric;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by dpolkovnikov on 15.03.17.
 */
@Service
public class ExerciseForm {
	@Min(-1)
	private long id;

	@NotNull(message = "Не указано имя")
	@Size(min = 1, max = 50, message = "Не введено название упражнения, или название слишком длинное")
	private String name;

	@Size(max = 500, message = "Описание слишком длинное")
	private String info;

	@NotNull(message = "Не выбрана метрика")
	@Min(value = 1, message = "Неправильно указана метрика")
	private int metricfk;

	@NotNull(message = "Версия не указана")
	@Min(value = 0, message = "Неправильная версия")
	private Integer version;

	public ExerciseForm() {
	}

	public ExerciseForm(long id, String name, String info, int metricfk) {
		this.id = id;
		this.name = name;
		this.info = info;
		this.metricfk = metricfk;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getMetricfk() {
		return metricfk;
	}

	public void setMetricfk(int metricfk) {
		this.metricfk = metricfk;
	}

	public Exercise getModel() throws MetricServiceException {
		Metric metric = new Metric();
		metric.setId(metricfk);
		Exercise res = new Exercise(this.getId(), this.getName(), this.getInfo(), metric);
		res.setVersion(version);
		return res;
	}
}

