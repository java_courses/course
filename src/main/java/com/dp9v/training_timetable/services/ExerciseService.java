package com.dp9v.training_timetable.services;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ExerciseServiceException;
import com.dp9v.training_timetable.models.converters.ExerciseConverter;
import com.dp9v.training_timetable.models.entity.ExerciseEntity;
import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.repositories.ExerciseRepository;
import com.dp9v.training_timetable.repositories.MetricRepository;
import com.dp9v.training_timetable.repositories.TrainingExerciseRepository;
import com.dp9v.training_timetable.services.interfaces.IExerciseService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public class ExerciseService implements IExerciseService {
	Logger logger = Logger.getLogger(ExerciseService.class);

	private ExerciseRepository         exerciseRepository;
	private TrainingExerciseRepository ter;
	private ExerciseConverter          converter;
	private MetricRepository           metricRepository;

	@Autowired
	public void setMetricRepository(MetricRepository metricRepository) {
		this.metricRepository = metricRepository;
	}

	@Autowired
	public void setExerciseRepository(ExerciseRepository exerciseRepository) {
		this.exerciseRepository = exerciseRepository;
	}

	@Autowired
	public void setTer(TrainingExerciseRepository ter) {
		this.ter = ter;
	}

	@Autowired
	public void setConverter(ExerciseConverter converter) {
		this.converter = converter;
	}

	/**
	 * Возвращает ID упражнения, соответстующего уникальному идентификатору
	 *
	 * @param identifier уникальный иденитификатор упражнения
	 * @return id упражнения
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public long getIdByIdentifier(UUID identifier) throws ExerciseServiceException {
		return getByIdentifier(identifier).getId();
	}

	/**
	 * Возвращает уникальный идентификатор упражнения, соответстующего ID
	 *
	 * @param id ID упражнения
	 * @return уникальный идентификатор упражнения
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_ADMIN"})
	public UUID getIdentifierBuId(long id) throws ExerciseServiceException {
		return getById(id).getIdentifier();
	}

	/**
	 * Возвращает список упражнений(exercise) в БД
	 *
	 * @return Список упражнений в базе
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<Exercise> all() throws ExerciseServiceException {
		try {
			List<ExerciseEntity> res = (List<ExerciseEntity>) exerciseRepository
					.findAll(new Sort(Sort.Direction.ASC, "id"));
			res = res.stream().filter(exerciseEntity -> exerciseEntity.getDeleteType() == 0)
			         .collect(Collectors.toList());
			return converter.entityListToPojoList(res);
		}
		catch (RuntimeException ex) {
			logger.error(ex);
			throw new ExerciseServiceException("Ошибка получения списка тренировок");
		}
	}

	/**
	 * Пакетное сохранение в БД списка упражнений, корректно сохраненные объекты удаляются из списка
	 *
	 * @param objects список упражнений для сохнаниея
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void insertAll(List<Exercise> objects) throws ExerciseServiceException {
		try {
			List<ExerciseEntity> entities = converter.pojoListToEntityList(objects);
			exerciseRepository.save(entities);
		}
		catch (RuntimeException ex) {
			logger.error(ex);
			throw new ExerciseServiceException("Ошибка загрузки списка тренировок");
		}
	}


	/**
	 * Возвращает объект упражнения, соответствующий ID
	 *
	 * @param id ID упражнения
	 * @return Объект упражнения, соответствующий ID
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Exercise getById(long id) throws ExerciseServiceException {
		try {
			return converter.mapEntityToPojo(exerciseRepository.findOne(id));
		}
		catch (RuntimeException ex) {
			logger.error("", ex);
			throw new ExerciseServiceException("Ошибка получения упраженения");
		}
	}

	/**
	 * Возвращает объект упражнения, соответствующий уникальному идентификатору
	 *
	 * @param identifier уникальный идентификатор упражнения
	 * @return Объект упражнения, соответствующий уникальному идентификатору
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Exercise getByIdentifier(UUID identifier) throws ExerciseServiceException {
		try {
			return converter.mapEntityToPojo(exerciseRepository.getByIdentifier(identifier));
		}
		catch (RuntimeException ex) {
			logger.trace(ex);
			throw new ExerciseServiceException("Ошибка получения упражнения");
		}
	}

	/**
	 * Удаляет упражнение с указанным ID из БД
	 *
	 * @param id ID записи для удаления
	 * @throws ExerciseServiceException Ошбка работы сервиса
	 */
	@Override
	public void delete(long id) throws ExerciseServiceException {
		try {
			long count = ter.findByDeleteTypeAndExercise_Id(0, id).size();
			if(count > 0)
				throw new ExerciseServiceException("Нельзя удалить это упражнение, есть связанные с ним тренировки");
			ExerciseEntity entity = exerciseRepository.findOne(id);
			entity.setDeleteType(1);
			exerciseRepository.save(entity);
		}
		catch (RuntimeException e) {
			logger.error(e);
			throw new ExerciseServiceException("Ошибка удаления записи");
		}
	}

	/**
	 * Сохраняет объект в БД
	 *
	 * @param exercise объект для сохранения
	 * @throws ExerciseServiceException Ошибка работы сервиса
	 */
	@Override
	public Exercise save(Exercise exercise) throws ExerciseServiceException {
		ExerciseEntity entity = converter.mapPojoToEntity(exercise);
		entity.setMetric(metricRepository.findOne(exercise.getMetric().getId()));
		try {
			if(exercise.getId() == 0) {
				entity = exerciseRepository.save(entity);
			} else {
				ExerciseEntity saveEntity = exerciseRepository.findOne(exercise.getId());
				if(saveEntity.getVersion() > entity.getVersion())
					throw new ExerciseServiceException("Упражнение было кем-то отредактирована. Обновите страницу");
				saveEntity.setMetric(entity.getMetric());
				saveEntity.setInfo(entity.getInfo());
				saveEntity.setName(entity.getName());
				entity = exerciseRepository.save(saveEntity);
			}
			return converter.mapEntityToPojo(entity);
		}
		catch (RuntimeException e) {
			logger.error(e);
			throw new ExerciseServiceException("Ошибка сохранения упражнения");
		}
	}
}
