package com.dp9v.training_timetable.models.pojo;


import java.util.UUID;

public class TrainingExercise {

	public  Long     id;
	private UUID     identifier;
	private Training training;
	private Exercise exercise;
	private Double   counter;
	private Boolean  isDone;
	private String   comment;
	private Integer  deleteType;
	private Integer  version;

	public TrainingExercise() {
	}

	public TrainingExercise(Long id, UUID identifier, Training training,
	                        Exercise exercise, Double counter, Boolean isDone, String comment) {
		this.id = id;
		this.identifier = identifier;
		this.training = training;
		this.exercise = exercise;
		this.counter = counter;
		this.isDone = isDone;
		this.comment = comment;
	}

	public TrainingExercise(UUID identifier, Training training, Exercise exercise, Double counter, Boolean isDone,
	                        String comment) {
		this.identifier = identifier;
		this.training = training;
		this.exercise = exercise;
		this.counter = counter;
		this.isDone = isDone;
		this.comment = comment;
	}


	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Training getTraining() {
		return training;
	}

	public void setTraining(Training training) {
		this.training = training;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}

	public Double getCounter() {
		return counter;
	}

	public void setCounter(Double counter) {
		this.counter = counter;
	}

	public Boolean getIsDone() {
		return isDone;
	}

	public void setIsDone(Boolean isDone) {
		this.isDone = isDone;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
