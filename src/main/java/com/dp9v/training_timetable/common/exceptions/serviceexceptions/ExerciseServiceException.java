package com.dp9v.training_timetable.common.exceptions.serviceexceptions;

/**
 * Created by dpolkovnikov on 13.03.17.
 */
public class ExerciseServiceException extends ServiceException {
	public ExerciseServiceException() {
	}

	public ExerciseServiceException(String message) {
		super(message);
	}

	public ExerciseServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExerciseServiceException(Throwable cause) {
		super(cause);
	}

	public ExerciseServiceException(String message, Throwable cause, boolean enableSuppression,
	                                boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
