<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<rapid:override name="title">Регистрация</rapid:override>
<rapid:override name="body">
    <div class="gray">
        <div class="container ">

            <form id="form-reg" class="form-reg" role="form" method="post" action="#">
                <h2 class="form-reg-heading">Регистрация</h2>
                <input type="email" class="form-control" placeholder="Email address" name="email" required>
                <input type="password" id="password" class="form-control"
                       placeholder="Password" name="password" required>
                <input type="password" id="password2" class="form-control" placeholder="Repeat password" required>
                <input type="text" class="form-control" placeholder="Name" name="name">
                <input type="date" class="form-control" placeholder="Birthday" name="birthday">
                <button id="btn-reg" class="btn btn-lg btn-primary btn-block btn-submit" >Зарегистрироваться</button>
                <a href="${pageContext.request.contextPath}/login" role="button">Авторизация</a>
            </form>

        </div>
    </div>
</rapid:override>

<%@ include file="base.jsp" %>