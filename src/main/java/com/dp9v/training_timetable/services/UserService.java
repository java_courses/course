package com.dp9v.training_timetable.services;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.models.converters.UserConverter;
import com.dp9v.training_timetable.models.entity.UserEntity;
import com.dp9v.training_timetable.models.pojo.User;
import com.dp9v.training_timetable.repositories.UserRepository;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public class UserService implements IUserService {
	private static Logger LOGGER = Logger.getLogger(UserService.class);

	private UserRepository userRepository;
	private UserConverter  converter;

	@Autowired
	public void setConverter(UserConverter converter) {
		this.converter = converter;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Возвращает ID пользователя, соответстующего уникальному идентификатору
	 *
	 * @param identifier уникальный иденитификатор пользователя
	 * @return id пользователя
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public long getIdByIdentifier(UUID identifier) throws UserServiceException {
		try{
			return userRepository.getByIdentifier(identifier).getId();
		}catch (RuntimeException ex){
			throw new UserServiceException("Пользователь не найден");
		}
	}

	/**
	 * Сохраняет существущий объект пользоваеля в БД
	 *
	 * @param user объект для сохранения
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public User save(User user) throws UserServiceException {
		UserEntity entity = converter.mapPojoToEntity(user);
		if(entity.getId()==null || user.getId() == 0) {
			entity = userRepository.save(entity);
			entity = userRepository.findOne(entity.getId());
		} else {
			throw new UserServiceException("Запрещено редактировать данные пользователя");
		}
		return converter.mapEntityToPojo(entity);
	}


	/**
	 * Проверяет на сущетвование пользователя с заданным e-mail
	 *
	 * @param email адрес, который необходимо проверить
	 * @return Результат проверки
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public boolean checkEmail(String email) {
		try {
			UserEntity us = userRepository.getByEmail(email);
			return us != null;
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			return false;
		}
	}

	/**
	 * Возвращает объект пользователя с указанными токеном
	 *
	 * @param token токен, который необходимо найти
	 * @return пользователь с указанным токеном
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public User getByToken(UUID token) throws UserServiceException {
		try {
			UserEntity user = userRepository.getByToken(token);
			if(user == null)
				throw new UserServiceException("Пользователь с таким токеном не найден");
			return converter.mapEntityToPojo(user);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new UserServiceException("Ошибка получения пользователя");
		}
	}

	/**
	 * Устанавливает записи с id=@id в поле с именем @type значение @value
	 *
	 * @param id    ID изменямой записи
	 * @param type  имя поля записи
	 * @param value устанавливаемое значение
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_ADMIN"})
	public void setValue(long id, String type, Object value) throws UserServiceException {
		try {
			UserEntity user = userRepository.findOne(id);
			if(type.equalsIgnoreCase("isactive")) {
				user.setIsActive((Boolean) value);
			} else if(type.equalsIgnoreCase("isadmin")) {
				user.setIsAdmin((Boolean) value);
			} else if(type.equalsIgnoreCase("getnotifications")) {
				user.setGetNotifications((Boolean) value);
			}
			userRepository.save(user);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new UserServiceException("Ошибка сохранения пользователя");
		}
	}

	/**
	 * Возвращает объект пользователя по его ID
	 *
	 * @param id ID пользователя
	 * @return объект пользователя соответствующий ID
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public User getById(long id) throws UserServiceException {
		try {
			UserEntity user = userRepository.findOne(id);
			if(user == null)
				throw new UserServiceException("Пользователь не найден");
			return converter.mapEntityToPojo(user);
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new UserServiceException("Ошибка получения пользователя");
		}
	}

	@Override
	public void delete(long id) throws UserServiceException {
		setValue(id, "isactive", false);
	}

	/**
	 * Возвращает объект пользователя по его email
	 *
	 * @param email ID пользователя
	 * @return объект пользователя соответствующий ID
	 * @throws UserServiceException Ошибка получения пользователя
	 */
	@Override
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public User getByEmail(String email) throws UserServiceException {
		try {
			UserEntity entity = userRepository.getByEmail(email);
			if(entity == null)
				throw new UserServiceException("Пользователь с таким адресом не найден");
			return converter.mapEntityToPojo(entity);
		}
		catch (RuntimeException e) {
			LOGGER.trace(e);
			throw new UserServiceException(e.getMessage());
		}
	}

	/**
	 * Возвращает список пользователей(User) в БД
	 *
	 * @return Список пользователей в базе
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<User> all() throws UserServiceException {
		try {
			return converter.entityListToPojoList(
					(List<UserEntity>) userRepository.findAll(new Sort(Sort.Direction.ASC, "id")));
		}
		catch (RuntimeException ex) {
			LOGGER.error(ex);
			throw new UserServiceException("Ошибка получения списка пользователей");
		}
	}

	/**
	 * Пакетная загрузка списка пользователей.
	 *
	 * @param objects Объект, содержащий в себе список пользователей
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void insertAll(List<User> objects) throws UserServiceException {
		try {
			userRepository.save(converter.pojoListToEntityList(objects));
		}
		catch (RuntimeException e) {
			LOGGER.error(e);
			throw new UserServiceException("Ошибка загрузки списка пользователей");
		}
	}

	/**
	 * Регистрирует указанного пользователя в системе, если это возможно
	 *
	 * @param user объект пользователя для регистрации
	 * @throws UserServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public User registerUser(User user) throws UserServiceException {
		if(checkEmail(user.getEmail())) {
			throw new UserServiceException("Пользователь с таким e-mail уже зарегистрирован");
		}
		return save(user);
	}


	/**
	 * Активация пользователя
	 *
	 * @param token токен пользователя для активации
	 * @throws UserServiceException Ошибка активации
	 */
	@Override
	public void activateUser(UUID token) throws UserServiceException {
		try {
			UserEntity user = userRepository.getByToken(token);
			if(user.getIsActive())
				throw new UserServiceException("Пользователь уже активирован");
			user.setIsActive(true);
			userRepository.save(user);
		}
		catch (RuntimeException ex) {
			LOGGER.trace(ex);
			throw new UserServiceException("Ошибка активации пользователя");
		}
	}
}
