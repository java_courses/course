package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.MetricServiceException;
import com.dp9v.training_timetable.models.pojo.Metric;
import org.springframework.stereotype.Service;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public interface IMetricService extends ServiceInterface<Metric>{
	Metric getMetricByType(String type) throws MetricServiceException;
	Metric save(Metric model) throws MetricServiceException;
}
