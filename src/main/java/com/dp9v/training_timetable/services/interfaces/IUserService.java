package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.models.pojo.User;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public interface IUserService extends ServiceInterface<User> {
	public long getIdByIdentifier(UUID identifier) throws UserServiceException;

	public User save(User user) throws UserServiceException;

	public boolean checkEmail(String email);

	public User getByToken(UUID token) throws UserServiceException;

	public void setValue(long id, String type, Object value) throws UserServiceException;

	public User getByEmail(String name) throws UserServiceException;

	public User registerUser(User user) throws UserServiceException;

	public void activateUser(UUID token) throws UserServiceException;
}
