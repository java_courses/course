package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.forms.ExerciseForm;
import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.services.interfaces.IExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dpolkovnikov on 15.03.17.
 */
@Controller
@RequestMapping(value = "/admin")
public class ExerciseController {
	IExerciseService exerciseService;


	@Autowired
	public void setExerciseService(IExerciseService exerciseService) {
		this.exerciseService = exerciseService;
	}

	@ResponseBody
	@RequestMapping(value = "/removeExercise", method = RequestMethod.POST)
	public Map<String, Object> removeExercise(@RequestParam("id") long id) {
		Map<String, Object> res = new HashMap<>(4);
		try {
			exerciseService.delete(id);
			res.put("success", true);
			res.put("id", id);
		}
		catch (ServiceException e) {
			res.put("success", false);
			res.put("message", e.getMessage());
		}
		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/saveExercise", method = RequestMethod.POST)
	public Map<String, Object> saveMetric(@Valid ExerciseForm form, BindingResult bindingResult) {
		Map<String, Object> res = new HashMap<>(4);
		if(bindingResult.hasErrors()) {
			res.put("success", false);
			res.put("message", bindingResult.getFieldError().getDefaultMessage());
		} else {
			try {
				Exercise exercise = form.getModel();
				exercise = exerciseService.save(exercise);
				res.put("success", true);
				res.put("object", exercise);
			}
			catch (ServiceException ex) {
				res.put("success", false);
				res.put("message", ex.getMessage());
			}
		}
		return res;
	}
}
