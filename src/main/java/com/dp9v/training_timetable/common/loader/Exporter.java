package com.dp9v.training_timetable.common.loader;


import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.loader.lists.*;
import com.dp9v.training_timetable.common.marshaller.XMLMarshaller;
import com.dp9v.training_timetable.repositories.*;
import com.dp9v.training_timetable.services.interfaces.ServiceInterface;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Класс реализующий многопоточную выгрузку объектов из БД
 *
 * Created by dpolkovnikov on 25.02.17.
 */
public class Exporter implements Runnable {

	private static          Logger  logger   = Logger.getLogger(Exporter.class);
	private static volatile boolean stopFlag = false;
	private String           dirPath;
	private String           tableName;
	private ServiceInterface service;

	/**
	 * Конструктор
	 *
	 * @param dirPath директрория в которую необходимо сохранить результирующий xml-файл
	 * @param tableName название таблицы для сохранения
	 */
	public Exporter(String dirPath, String tableName, ServiceInterface service) {
		this.dirPath = dirPath;
		this.tableName = tableName;
	}

	/**
	 * Запуст потока сохранения объетов в xml
	 */
	@Override
	public void run() {
		AbstractModelList list = null;
		try{
			switch(tableName) {
				case UserRepository.TABLE_NAME:
					list = new Users();
					((Users)list).fillUsers(service.all());
					break;
				case TrainingExerciseRepository.TABLE_NAME:
					list = new TrainingExercises();
					((TrainingExercises)list).fillTrainingExercises(service.all());
					break;
				case TrainingRepository.TABLE_NAME:
					list = new Trainings();
					((Trainings)list).fillTrainings(service.all());
					break;
				case MetricRepository.TABLE_NAME:
					list = new Metrics();
					((Metrics)list).fillMetrics(service.all());
					break;
				case ExerciseRepository.TABLE_NAME:
					list = new Exercises();
					((Exercises)list).fillExercises(service.all());
					break;
			}
		}catch (ServiceException ex){
			logger.error(ex);
			stopFlag = true;
		}

		Path path = Paths.get(dirPath, tableName + ".xml");
		if (!stopFlag){
			try {
				XMLMarshaller.saveObject(path.toFile(), list);
			}
			catch (JAXBException ex) {
				stopFlag = true;
				logger.error(ex);
			}
		}

	}
}
