package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingExerciseServiceException;
import com.dp9v.training_timetable.forms.TrainingExerciseForm;
import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;
import com.dp9v.training_timetable.services.interfaces.IExerciseService;
import com.dp9v.training_timetable.services.interfaces.ITrainingExerciseService;
import com.dp9v.training_timetable.services.interfaces.ITrainingService;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dpolkovnikov on 23.03.17.
 */
@Controller("training")
@RequestMapping("/training")
public class TrainingController {

	ITrainingExerciseService trainingExerciseService;
	ITrainingService         trainingService;
	IExerciseService         exerciseService;
	IUserService             userService;

	@Autowired
	public void setTrainingService(ITrainingService trainingService) {
		this.trainingService = trainingService;
	}

	@Autowired
	public void setExerciseService(IExerciseService exerciseService) {
		this.exerciseService = exerciseService;
	}

	@Autowired
	public void setTrainingExerciseService(
			ITrainingExerciseService trainingExerciseService) {
		this.trainingExerciseService = trainingExerciseService;
	}

	@Autowired
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}


	@RequestMapping(value = "{ID}", method = RequestMethod.GET)
	public String showExercises(@PathVariable(value = "ID") Long id,
	                            Model model, Principal principal) {
		try {
			if(!trainingService.checkAccess(id, principal.getName()))
				throw new ServiceException("У вас нет прав для просмотра списка упражений");
			List<TrainingExercise> list      = trainingExerciseService.findForTraining(id);
			List<Exercise>         exercises = exerciseService.all();
			model.addAttribute("exercises", list);
			model.addAttribute("allExercises", exercises);
			model.addAttribute("training", id);
		}
		catch (ServiceException e) {
			return "error403";
		}
		return "exercises";
	}


	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestParam(name = "id") Long id, Principal principal) {
		Map<String, Object> res = new HashMap<>(4);
		try {
			if(!trainingExerciseService.checkAccess(id, principal.getName()))
				throw new ServiceException("У вас нет прав для удаления записи");
			trainingExerciseService.delete(id);
			res.put("id", id);
			res.put("success", true);
		}
		catch (ServiceException e) {
			res.put("message", e.getMessage());
			res.put("success", false);
		}
		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public Map<String, Object> insert(@Valid TrainingExerciseForm form,
	                                  BindingResult bindingResult,
	                                  Principal principal) {
		Map<String, Object> res = new HashMap<>(4);
		if(bindingResult.hasErrors()) {
			res.put("success", false);
			res.put("message", bindingResult.getFieldError().getDefaultMessage());
		} else {
			try {
				TrainingExercise model = form.getModel();
				model = trainingExerciseService.save(model, principal.getName());
				res.put("success", true);
				res.put("object", model);
			}
			catch (TrainingExerciseServiceException e) {
				res.put("success", false);
				res.put("message", e.getMessage());
			}
		}
		return res;
	}

}
