package com.dp9v.training_timetable.services;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingServiceException;
import com.dp9v.training_timetable.models.converters.TrainingConverter;
import com.dp9v.training_timetable.models.entity.TrainingEntity;
import com.dp9v.training_timetable.models.entity.TrainingExerciseEntity;
import com.dp9v.training_timetable.models.pojo.Training;
import com.dp9v.training_timetable.repositories.TrainingExerciseRepository;
import com.dp9v.training_timetable.repositories.TrainingRepository;
import com.dp9v.training_timetable.services.interfaces.ITrainingService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by dpolkovnikov on 24.02.17.
 */
@Service
public class TrainingService implements ITrainingService {
	private static Logger logger = Logger.getLogger(TrainingService.class);
	private TrainingConverter          converter;
	private TrainingRepository         trainingRepository;
	private TrainingExerciseRepository ter;

	@Autowired
	public void setTer(TrainingExerciseRepository ter) {
		this.ter = ter;
	}

	@Autowired
	public void setConverter(TrainingConverter converter) {
		this.converter = converter;
	}

	@Autowired
	public void setTrainingRepository(TrainingRepository trainingRepository) {
		this.trainingRepository = trainingRepository;
	}

	/**
	 * Возвращает объект тренировки, соответствующий ID
	 *
	 * @param id ID записи в БД
	 * @return Объект тренировки, соответствующий ID
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Training getById(long id) throws TrainingServiceException {
		try {
			return converter.mapEntityToPojo(trainingRepository.findOne(id));
		}
		catch (RuntimeException ex) {
			logger.error("", ex);
			throw new TrainingServiceException("Тренировка не найдена");
		}
	}

	/**
	 * Устанавливает записи с указанным id и всем связанным с ней записаям delete_type=1
	 *
	 * @param id ID записи в БД
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Override
	public void delete(long id) throws TrainingServiceException {
		try {
			List<TrainingExerciseEntity> entities = ter.findByDeleteTypeAndTraining_Id(0, id);
			entities.forEach(trainingExerciseEntity -> trainingExerciseEntity.setDeleteType(1));
			ter.save(entities);
			TrainingEntity trainingEntity = trainingRepository.findOne(id);
			trainingEntity.setDeleteType(1);
			trainingRepository.save(trainingEntity);
		}
		catch (RuntimeException ex) {
			logger.trace(ex);
			throw new TrainingServiceException("Ошибка сохранения тренировки");
		}
	}

	/**
	 * Возвращает объект тренировки, соответствующий идентификатору
	 *
	 * @param identifier уникальный идентификатор тренировки
	 * @return Объект тренировки, соответствующий идентификатору
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public Training getByIdentifier(UUID identifier) throws TrainingServiceException {
		try {
			return converter.mapEntityToPojo(trainingRepository.getByIdentifier(identifier));
		}
		catch (RuntimeException ex) {
			logger.trace(ex);
			throw new TrainingServiceException("Ошибка получения записи по идентификатору");
		}
	}

	/**
	 * Возвращает ID тренировки, соответстующего уникальному идентификатору
	 *
	 * @param identifier уникальный иденитификатор тренировки
	 * @return id тренировки
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public long getIdByIdentifier(UUID identifier) throws TrainingServiceException {
		return getByIdentifier(identifier).getId();
	}

	/**
	 * Получение списка всех тренировок указанного пользователя
	 *
	 * @param id логин пользователя
	 * @return список тренировок
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<Training> allByUserId(Long id) throws TrainingServiceException {
		try {
			List<TrainingEntity> trainings = trainingRepository.findByDeleteTypeAndUser_Id(0, id);
			return converter.entityListToPojoList(trainings);
		}
		catch (RuntimeException e) {
			logger.error(e);
			throw new TrainingServiceException("Ошибка получения списка тренировок");
		}
	}

	@Override
	public Boolean checkAccess(Long id, String email) throws TrainingServiceException {
		try {
			TrainingEntity trainingEntity = trainingRepository.findOne(id);
			return trainingEntity.getUser().getEmail().equalsIgnoreCase(email);
		}
		catch (RuntimeException ex) {
			logger.trace(ex);
			throw new TrainingServiceException("Ошибка доступа");
		}
	}

	/**
	 * Сохраняет новую запись в БД или перезаписывает предыдущую
	 *
	 * @param model объект для сохранения
	 * @return Ссылка на сохраненный объект
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Override
	public Training save(Training model) throws TrainingServiceException {
		if(model.getDate().before(java.sql.Date.valueOf(LocalDate.now())))
			throw new TrainingServiceException("Нельзя запланировать тренировку на этот день");
		try {
			if(model.getId() == 0) {
				TrainingEntity entity = trainingRepository.save(converter.mapPojoToEntity(model));
				return converter.mapEntityToPojo(entity);
			} else {
				TrainingEntity entity = trainingRepository.findOne(model.getId());
				if(!entity.getUser().getId().equals(model.getUser().getId()))
					throw new TrainingServiceException("У вас нет прав на редактирование этой записи");
				if(!entity.getVersion().equals(model.getVersion()))
					throw new TrainingServiceException(
							"Данная запись была кем-то отредактирована. Пожалуйста обновите страницу");
				entity.setBeginTime(model.getBeginTime());
				entity.setDate(model.getDate());
				entity = trainingRepository.save(entity);
				return converter.mapEntityToPojo(entity);
			}
		}
		catch (RuntimeException ex) {
			logger.error(ex);
			throw new TrainingServiceException("Ошибка сохранения записи");
		}

	}

	/**
	 * Возвращает список тренировок(trainings) в БД
	 *
	 * @return Список тренировок в базе
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Secured({"ROLE_USER", "ROLE_ADMIN"})
	public List<Training> all() throws TrainingServiceException {
		try {
			List<TrainingEntity> res = trainingRepository.findByDeleteTypeOrderByBeginTimeAsc(0);
			return converter.entityListToPojoList(res);
		}
		catch (RuntimeException e) {
			logger.error("", e);
			throw new TrainingServiceException("Ошибка получения списка тренировок");
		}
	}

	/**
	 * Пакетно загружает в БД список тренировок
	 *
	 * @param objects Список тренировок для загрузки
	 * @throws TrainingServiceException Ошибка работы сервиса
	 */
	@Override
	@Secured({"ROLE_ADMIN"})
	public void insertAll(List<Training> objects) throws TrainingServiceException {
		try {
			trainingRepository.save(converter.pojoListToEntityList(objects));
		}
		catch (RuntimeException e) {
			logger.error(e);
			throw new TrainingServiceException("Ошибка загрузки данных");
		}
	}
}
