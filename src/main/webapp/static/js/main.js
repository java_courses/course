$(document).ready(function () {

    $("#btn-reg").click(function (e) {
        e.preventDefault();
        pass1 = $("#password").val();
        pass2 = $("#password2").val();

        if (pass1 !== pass2) {
            alert("Пароли не совпадают");
            return;
        }

        dataString = $("#form-reg").serialize();
        link = $("#form-reg").attr("action");
        sendAJAX(
            link,
            dataString,
            function (data, textStatus, jqXHR) {
                if (data.success)
                    showMessage("Регистрация прошла успешно", data.message);
                else
                    showMessage("Ошибка", data.message);
            }
        );
    });


    $("#btn-signin").click(function (e) {
        e.preventDefault();
        dataString = $("#form-signin").serialize();
        link = $("#form-signin").attr("action");
        sendAJAX(
            link,
            dataString,
            function (data, textStatus, jqXHR) {
                if (data.success) {
                    window.location.replace(data.message);
                } else {
                    showMessage("Ошибка", data.message);
                }
            }
        );
    });


    $("#adminTabs").find("a").click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(".ajaxCheckBox").change(function (e) {
        id = $(this).data('id');
        name = $(this).attr('name');
        LINK = "/admin/setUserValue";
        value = this.checked;
        sendAJAX(
            LINK,
            {id: id, name: name, value: value},
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message)
            }
        );
    });

    $("table").on('click', '.btn-rm', function (e) {
        id = $(this).data('id');
        link = $(this).data('link');
        sendAJAX(
            link,
            {id: id},
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message);
                else {
                    $("#row" + data.id).remove();
                    showMessage("ОК", "Запись успешно удалена и вы не сможете её восстановить");
                }
            }
        );
    });
});

/**
 * Блок отвечающий за работу с метриками
 */
$(document).ready(function () {

    $("#metricForm").on('submit', function (e) {
        e.preventDefault();
        form = $(this).closest("form");
        link = form.attr("action");
        data = form.serialize();
        sendAJAX(
            link,
            data,
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message);
                else {
                    addMetricToTable(data.object);
                    showMessage("ОК", "Запись сохранена упешно");
                }
            }
        );
    });

    $('table').on('click', '.btn-metric-edit', function (e) {
        row = $(this).closest("tr");
        id = $(row).find(".idValue").text();
        type = $(row).find(".typeValue").text();
        version = $(this).data('version');
        showMetricPopup(id, version, type);
    });

    $(".btn-metric-create").click(function (e) {
        showMetricPopup(0, 0, null);
    });

    function showMetricPopup(id, version, type) {
        form = $('#metricForm');
        form.find('input[name=id]').val(id);
        form.find('input[name=type]').val(type);
        form.find('input[name=version]').val(version);
        $('#metricModal').modal("show");
    }

    function addMetricToTable(object) {
        table = $("#metricTab").find("table");
        $("#row" + object.id).remove();

        $('#metricModal').modal("hide");
        table.find("tr:last").before("<tr id='row" + object.id + "' class='row-metric'>" +
            "<td class='idValue'>" + object.id + "</td>" +
            "<td class='typeValue' >" + object.type + "</td>" +
            "<td><button class='btn btn-rm' data-id='" + object.id + "' data-link='/admin/removeMetric'>Удалить</button></td>" +
            "<td><button class='btn btn-metric-edit' data-version='" + object.version + "'>Редактировать</button></td> </tr>");
    }
});

/**
 * Блок отвечающий за работу с упражениями
 */
$(document).ready(function () {

    $("#exerciseForm").on('submit', function (e) {
        e.preventDefault();
        form = $(this).closest("form");
        link = form.attr("action");
        data = form.serialize();
        sendAJAX(
            link,
            data,
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message);
                else {
                    addExerciseToTable(data.object);
                    showMessage("ОК", "Запись сохранена упешно");
                }
            }
        );
    });

    $('table').on('click', ".btn-exercise-edit", function (e) {
        row = $(this).closest("tr");
        version = $(this).data('version');
        id = $(row).find(".idValue").text();
        name = $(row).find(".nameValue").text();
        info = $(row).find(".infoValue").text();
        metricFk = $(row).find(".metricValue").data("id");
        showExercisePopup(id, version, name, info, metricFk);
    });

    $(".btn-exercise-create").click(function (e) {
        showExercisePopup(0, 0, "", "", 0);
    });

    function addExerciseToTable(object) {
        table = $("#exercisesTab").find("table");
        $("#row" + object.id).remove();
        table.find("tr:last").before(
            "<tr id='row" + object.id + "'>" +
            "<td class='idValue'>" + object.id + "</td>" +
            "<td class='nameValue'>" + object.name + "</td>" +
            "<td class='infoValue'>" + object.info + "</td>" +
            "<td class='metricValue' data-id='" + object.metric.id + "'>" + object.metric.type + "</td>" +
            "<td><button class='btn btn-rm' data-id='" + object.id + "' data-link='/admin/removeExercise'>Удалить</button></td>" +
            "<td><button class='btn btn-exercise-edit' data-version='" + object.version + "' >Редактировать</button></td></tr>");
        $('#exerciseModal').modal("hide");

    }

    function showExercisePopup(id, version, name, info, metricFK) {
        metrics = getMetrics();
        form = $('#exerciseForm');
        form.find('input[name=id]').val(id);
        form.find('input[name=version]').val(version);
        form.find('input[name=name]').val(name);
        form.find('textarea[name=info]').val(info);
        combobox = form.find("select[name=metricfk]");
        combobox.empty();
        metrics.forEach(function (element) {
            combobox.append($("<option value='" + element.id + "'>" + element.type + "</option>"));
        });
        combobox.find("[value='" + metricFK + "']").attr("selected", "selected");
        $('#exerciseModal').modal("show");
    }

    function getMetrics() {
        metrics = [];
        rows = $(".row-metric");
        rows.each(function (i, element) {
            id = $(element).find(".idValue").text();
            type = $(element).find(".typeValue").text();
            metrics.push({id: id, type: type});
        });
        return metrics;
    }
});

/**
 * Блок отвечающий за работу со списком тренировок
 */
$(document).ready(function () {

    $("#trainingForm").on('submit', function (e) {
        e.preventDefault();
        form = $(this).closest("form");
        link = form.attr("action");
        data = form.serialize();
        sendAJAX(
            link,
            data,
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message);
                else {
                    addTrainingToTable(data.object);
                    showMessage("ОК", "Запись сохранена упешно");
                }
            }
        );
    });

    $('table').on('click', '.btn-training-edit', function (e) {
        version = $(this).data('version');
        row = $(this).closest("tr");
        id = $(row).find(".idValue").text();
        date = $(row).find(".dateValue").text();
        beginTime = $(row).find(".beginTimeValue").text().substring(0, 5);
        showTrainingPopup(id, version, date, beginTime);
    });

    $(".btn-training-create").click(function (e) {
        showTrainingPopup(0, 0, null, null);
    });

    function showTrainingPopup(id, version, date, time) {
        form = $('#trainingForm');
        form.find('input[name=id]').val(id);
        form.find('input[name=version]').val(version);
        form.find('input[name=date]').val(date);
        form.find('input[name=beginTime]').val(time);
        $('#trainingModal').modal("show");
    }

    function addTrainingToTable(object) {
        table = $("#futureTab").find("table");
        $("#row" + object.id).remove();
        $('#trainingModal').modal("hide");
        table.find("tr:last").before("<tr id='row" + object.id + "'>" +
            "<td class='idValue'>" + object.id + "</td>" +
            "<td class='dateValue'>" + object.date + "</td>" +
            "<td class='beginTimeValue'>" + object.beginTime + "</td>" +
            "<td><button class='btn btn-rm' data-id='" + object.id + "' data-link='/timetable/delete'>Удалить</button></td>" +
            "<td><button class='btn btn-training-edit' data-version='" + object.version + "'>Редактировать</button></td>" +
            "<td><a href='/training/" + object.id + "' class='btn btn-training-edit'>Просмотр</a></td>" +
            "</tr>");
    }
});

/**
 * Блок отвечающий за работу со списком упражнений для тренировки
 */
$(document).ready(function () {

    $("#trainingExerciseForm").on('submit', function (e) {
        e.preventDefault();
        form = $(this).closest("form");
        link = form.attr("action");
        data = form.serialize();
        sendAJAX(
            link,
            data,
            function (data, textStatus, jqXHR) {
                if (!data.success)
                    showMessage("Ошибка", data.message);
                else {
                    showMessage("ОК", "Запись сохранена упешно");
                    addTEToTable(data.object);
                }
            }
        );
    });

    $('table').on('click', '.btn-trainingExercise-edit', function (e) {
        row = $(this).closest("tr");
        version = $(this).data('version');
        exercise = $(this).data('exercise');
        id = $(row).find(".idValue").text();
        comment = $(row).find(".commentValue").text();
        counter = $(row).find(".counterValue").text();
        showTrainingExercisePopup(id, version, exercise, counter, comment);
    });

    $(".btn-trainingExercise-create").click(function (e) {
        showTrainingExercisePopup(0, 0, 0, "", "");
    });

    function showTrainingExercisePopup(id, version, exerise, counter, comment) {
        form = $('#trainingExerciseForm');
        form.find('input[name=id]').val(id);
        form.find('input[name=version]').val(version);
        form.find("[value='" + exerise + "']").attr("selected", "selected");
        form.find('input[name=comment]').val(comment);
        form.find('input[name=counter]').val(counter);
        $('#trainingExerciseModal').modal("show");
    }

    function addTEToTable(object) {
        table = $("table");
        $("#row" + object.id).remove();
        table.find("tr:last").before("<tr id='row" + object.id + "'>" +
            "<td class='idValue'>" + object.id + "</td>" +
            "<td>" + object.exercise.name + "</td>" +
            "<td class='counterValue'>" + object.counter + "</td>" +
            "<td>" + object.exercise.metric.type + "</td>" +
            "<td class='commentValue'>" + object.comment + "</td>" +
            "<td>" +
            "<button class='btn btn-rm' data-id='" + object.id + "' data-link='/training/delete'>Удалить</button>" +
            "</td>" +
            "<td><button class='btn btn-trainingExercise-edit' data-version='" + object.version + "' " +
            "data-exercise='" + object.exercise.id + "'>Редактировать</button> </td></tr>");
        $('#trainingExerciseModal').modal("hide");
    }
});


function showMessage(title, message) {
    $('#messageTitle').text(title);
    $('#messageText').text(message);
    $('#messageModal').modal("show");
}

function errorAjax(jqXHR, textStatus, errorThrown) {
    showMessage("Ошибка", "Ошибка обработки запроса");
}

function disableSubmit() {
    $('.btn-submit').attr("disabled", true);
}

function enableSubmit() {
    $('.btn-submit').attr("disabled", false);
}

function sendAJAX(link, data, successF, errorF, beforeF, afterF) {
    if (errorF === undefined) {
        errorF = errorAjax;
    }
    if (beforeF === undefined) {
        beforeF = disableSubmit;
    }
    if (afterF === undefined) {
        afterF = enableSubmit;
    }
    $.post({
        url: link,
        data: data,
        dataType: "json",
        success: successF,
        error: errorF,
        complete: afterF,
        beforeSend: beforeF
    });
}
