package com.dp9v.training_timetable.models.pojo;

import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

public class Training {

	public  Long    id;
	private UUID    identifier;
	private User    user;
	private Date    date;
	private Time    beginTime;
	private Integer deleteType;
	private Integer version;

	public Training() {
	}

	public Training(long id, UUID identifier, User user, Date date, Time beginTime) {
		this.id = id;
		this.identifier = identifier;
		this.user = user;
		this.date = date;
		this.beginTime = beginTime;
	}

	public Training(UUID identifier, User user, Date date, Time beginTime) {
		this.identifier = identifier;
		this.user = user;
		this.date = date;
		this.beginTime = beginTime;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public java.sql.Time getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(java.sql.Time beginTime) {
		this.beginTime = beginTime;
	}

}
