package com.dp9v.training_timetable.models.pojo;

public class Metric {

	public  long    id;
	private String  type;
	private Integer deleteType;
	private Integer version;

	public Metric(String type) {
		this.type = type;
	}

	public Metric() {
	}

	public Metric(long id, String type) {
		this.id = id;
		this.type = type;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
