package com.dp9v.training_timetable.common.loader.lists;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlTransient
public abstract class AbstractModelList<M> {
	private ArrayList<M> items;

	public AbstractModelList(ArrayList<M> items) {
		this.items = items;
	}

	public AbstractModelList() {
		items = new ArrayList<M>();
	}

	@XmlElement(name = "item")
	public ArrayList<M> getItems() {
		return items;
	}

	public void setItems(ArrayList<M> items) {
		this.items = items;
	}

	public void putItem(M item){
		this.items.add(item);
	}

	public LinkedList<M> getLinkedList(){
		LinkedList<M> list = new LinkedList<M>();
		list.addAll(items);
		return list;
	}

	public int size(){
		return items.size();
	}

	public void clear(){
		items.clear();
	}

	public ArrayList<M> subList(int begin, int size){
		int fin = begin+size>=items.size() ? items.size() : begin + size;
		return new ArrayList<M>(items.subList(begin, fin));
	}


}
