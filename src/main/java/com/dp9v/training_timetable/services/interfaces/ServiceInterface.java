package com.dp9v.training_timetable.services.interfaces;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import org.springframework.security.access.annotation.Secured;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 05.03.17.
 */
public interface ServiceInterface<T> {
	List<T> all() throws ServiceException;
	void insertAll(List<T> objects) throws ServiceException;
	T getById(long id) throws ServiceException;
	void delete(long id) throws ServiceException;
}
