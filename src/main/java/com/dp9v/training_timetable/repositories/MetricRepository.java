package com.dp9v.training_timetable.repositories;

import com.dp9v.training_timetable.models.entity.ExerciseEntity;
import com.dp9v.training_timetable.models.entity.MetricEntity;
import com.dp9v.training_timetable.models.entity.TrainingEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public interface MetricRepository extends PagingAndSortingRepository<MetricEntity, Long> {
	public final String TABLE_NAME = "metrics";

	MetricEntity getByType(String type);

}
