<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--@elvariable id="exercises" type="java.util.List<com.dp9v.training_timetable.models.pojo.TrainingExercise>"--%>


<rapid:override name="title">Список тренировок</rapid:override>

<rapid:override name="content">
    <div class="starter-template">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Упражнение</th>
                    <th>Результат выполнения</th>
                    <th>Счетчик</th>
                    <th>Комментарий</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${exercises}" var="exercise">
                    <tr id="row${exercise.id}">
                        <td class="idValue"><c:out value="${exercise.id}"/></td>
                        <td><c:out value="${exercise.exercise.name}"/></td>
                        <td class="counterValue"><c:out value="${exercise.counter}"/></td>
                        <td><c:out value="${exercise.exercise.metric.type}"/></td>
                        <td class="commentValue"><c:out value="${exercise.comment}"/></td>
                        <td>
                            <button class="btn btn-rm" data-id="${exercise.id}"
                                    data-link="<c:url value="/training/delete"/>">Удалить
                            </button>
                        </td>
                        <td>
                            <button class="btn btn-trainingExercise-edit"
                                    data-version="${exercise.version}"
                                    data-exercise="${exercise.exercise.id}">Редактировать
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="7">
                        <button class="btn btn-trainingExercise-create">Добавить</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <jsp:include page="popups/trainingExercisePopup.jsp" flush="true"/>
</rapid:override>


<%@ include file="/jsp/base.jsp" %>