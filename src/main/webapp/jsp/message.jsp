<%@ taglib uri="http://www.rapid-framework.org.cn/rapid" prefix="rapid" %>
<rapid:override name="title"><%= request.getAttribute("title") %>
</rapid:override>

<rapid:override name="body">
    <div class="container gray">

        <div class="jumbotron">
            <h1><%= request.getAttribute("title") %>
            </h1>
            <p class="lead"><%= request.getAttribute("message") %>
            </p>
            <p><a class="btn btn-lg btn-success" href="<%= request.getAttribute("href") %>" role="button">
                <%= request.getAttribute("hrefMessage") %>
            </a></p>
        </div>

    </div>
</rapid:override>


<%@ include file="/jsp/base.jsp" %>