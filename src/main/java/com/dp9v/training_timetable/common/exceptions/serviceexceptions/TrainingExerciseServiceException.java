package com.dp9v.training_timetable.common.exceptions.serviceexceptions;

/**
 * Created by dpolkovnikov on 13.03.17.
 */
public class TrainingExerciseServiceException extends ServiceException {
	public TrainingExerciseServiceException() {
	}

	public TrainingExerciseServiceException(String message) {
		super(message);
	}

	public TrainingExerciseServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public TrainingExerciseServiceException(Throwable cause) {
		super(cause);
	}

	public TrainingExerciseServiceException(String message, Throwable cause, boolean enableSuppression,
	                                        boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
