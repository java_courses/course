<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="messageTitle">Название модали</h4>
            </div>
            <div class="modal-body">
                <p id="messageText">One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div>
</div>