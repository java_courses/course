package com.dp9v.training_timetable.forms;

import com.dp9v.training_timetable.models.pojo.User;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

/**
 * Created by dpolkovnikov on 07.03.17.
 */
public class RegistrationForm {
	public final String EMAIL_REGEX = "^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$";


	@NotNull
	@Size(max = 250, min = 1, message = "Ошибка ввода имени")
	private String name;

	@Size(max = 50, min = 1, message = "Ошибка ввода e-mail")
	@NotNull
	@Email(message = "Неверный формат")
	private String email;

	@NotNull
	@Size(max = 50, min = 5, message = "Пароль слишком короткий или длинный")
	private String password;
	private String birthday;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public User getUser() {
		User user = new User(name, email, password, birthday.isEmpty() ? null : Date.valueOf(birthday));
		return user;
	}
}
