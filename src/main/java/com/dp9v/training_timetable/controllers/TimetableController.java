package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.exceptions.serviceexceptions.TrainingServiceException;
import com.dp9v.training_timetable.services.auth.AuthUserService;
import com.dp9v.training_timetable.forms.TrainingForm;
import com.dp9v.training_timetable.models.pojo.Training;
import com.dp9v.training_timetable.services.interfaces.ITrainingService;
import com.dp9v.training_timetable.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.Principal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
@Controller("timetable")
@RequestMapping(value = "/timetable")
public class TimetableController {

	private ITrainingService trainingService;
	private IUserService     userService;

	@Autowired
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	@Autowired
	public void setService(ITrainingService trainingService) {
		this.trainingService = trainingService;
	}


	@RequestMapping(value = "", method = RequestMethod.GET)
	public String showTraining(Model model, Principal principal) {
		AuthUserService.UserDetailsImpl currentUser =
				(AuthUserService.UserDetailsImpl) ((Authentication) principal).getPrincipal();
		try {
			List<Training> trainings       = trainingService.allByUserId(currentUser.getUser().getId());
			List<Training> pastTrainings   = new ArrayList<>();
			List<Training> futureTrainings = new ArrayList<>();
			for(Training training : trainings) {
				if(training.getDate().before(Date.valueOf(LocalDate.now())))
					pastTrainings.add(training);
				else
					futureTrainings.add(training);
			}
			model.addAttribute("afterTraining", futureTrainings);
			model.addAttribute("pastTrainings", pastTrainings);
		}
		catch (ServiceException exception) {
			exception.printStackTrace();
		}
		return "timetable";
	}

	@ResponseBody
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public Map<String, Object> insert(@Valid TrainingForm form,
	                                  BindingResult bindingResult,
	                                  Principal principal) {
		AuthUserService.UserDetailsImpl currentUser =
				(AuthUserService.UserDetailsImpl) ((Authentication) principal).getPrincipal();
		Map<String, Object> res = new HashMap<>(4);
		if(bindingResult.hasErrors()) {
			res.put("success", false);
			res.put("message", bindingResult.getFieldError().getDefaultMessage());
		} else {
			try {
				Training training = form.getModel();
				training.setUser(currentUser.getUser());
				training = trainingService.save(training);
				res.put("success", true);
				res.put("object", training);
			}
			catch (TrainingServiceException e) {
				res.put("success", false);
				res.put("message", e.getMessage());
			}
		}
		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestParam(name = "id") Long id, Principal principal) {
		Map<String, Object> res = new HashMap<>(4);
		try {
			if(!trainingService.checkAccess(id, principal.getName()))
				throw new ServiceException("У вас нет прав на редактировние этой записи");
			trainingService.delete(id);
			res.put("id", id);
			res.put("success", true);
		}
		catch (ServiceException e) {
			res.put("success", false);
			res.put("message", e.getMessage());
		}
		return res;
	}

}
