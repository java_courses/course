package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.models.pojo.Metric;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by dpolkovnikov on 02.03.17.
 */
@XmlRootElement(name = "metric")
public class MetricXML implements XMLInterface {
	private String type;

	public MetricXML() {
	}

	public MetricXML(Metric metric) {
		this.type = metric.getType();
	}

	public Object getModel() {
		Metric res = new Metric();
		res.setType(type);
		return res;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
