package com.dp9v.training_timetable.models.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 20.03.17.
 */
@Entity
@Table(name = "trainings", schema = "public", catalog = "TrainingTimetable")
public class TrainingEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "main_seq")
	@SequenceGenerator(name = "main_seq", sequenceName = "main_seq", allocationSize = 1)
	private Long id;

	@Column
	private Date date;

	@Column(name = "begin_time")
	private Time beginTime;

	@Column(updatable = false, insertable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private UUID identifier;

	@Column(name = "delete_type", insertable = false)
	private Integer deleteType;

	@Version
	@Column
	private Integer version;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userfk")
	private UserEntity user;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "training")
	private Set<TrainingExerciseEntity> trainingExercises = new HashSet<>();

	public TrainingEntity() {
	}

	public Set<TrainingExerciseEntity> getTrainingExercises() {
		return trainingExercises;
	}

	public void setTrainingExercises(
			Set<TrainingExerciseEntity> trainingExercises) {
		this.trainingExercises = trainingExercises;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	@Override
	public boolean equals(Object o) {
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		TrainingEntity that = (TrainingEntity) o;

		if(id != null ? !id.equals(that.id) : that.id != null)
			return false;
		if(date != null ? !date.equals(that.date) : that.date != null)
			return false;
		if(beginTime != null ? !beginTime.equals(that.beginTime) : that.beginTime != null)
			return false;
		if(identifier != null ? !identifier.equals(that.identifier) : that.identifier != null)
			return false;
		if(deleteType != null ? !deleteType.equals(that.deleteType) : that.deleteType != null)
			return false;
		if(version != null ? !version.equals(that.version) : that.version != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + (beginTime != null ? beginTime.hashCode() : 0);
		result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
		result = 31 * result + (deleteType != null ? deleteType.hashCode() : 0);
		result = 31 * result + (version != null ? version.hashCode() : 0);
		return result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Time beginTime) {
		this.beginTime = beginTime;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
