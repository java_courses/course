package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;

/**
 * Created by dpolkovnikov on 03.03.17.
 */
public interface XMLInterface {
	public Object getModel() throws ServiceException;
}
