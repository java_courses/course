package com.dp9v.training_timetable.common.exceptions.serviceexceptions;

/**
 * Created by dpolkovnikov on 13.03.17.
 */
public class MetricServiceException extends ServiceException {
	public MetricServiceException() {
	}

	public MetricServiceException(String message) {
		super(message);
	}

	public MetricServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public MetricServiceException(Throwable cause) {
		super(cause);
	}

	public MetricServiceException(String message, Throwable cause, boolean enableSuppression,
	                              boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
