package com.dp9v.training_timetable.models.converters;

import com.dp9v.training_timetable.models.entity.ExerciseEntity;
import com.dp9v.training_timetable.models.pojo.Exercise;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Service
public class ExerciseConverter {
	private final static MapperFacade mapper;

	static {
		final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(Exercise.class, ExerciseEntity.class)
		             .byDefault().register();
		mapper = mapperFactory.getMapperFacade();
	}

	public ExerciseConverter() {
	}

	public Exercise mapEntityToPojo(final ExerciseEntity exercise) {
		Exercise pDto = mapper.map(exercise, Exercise.class);
		return pDto;
	}

	public ExerciseEntity mapPojoToEntity(final Exercise exercise) {
		ExerciseEntity pDto = mapper.map(exercise, ExerciseEntity.class);
		return pDto;
	}

	public List<ExerciseEntity> pojoListToEntityList(List<Exercise> exercises) {
		ArrayList<ExerciseEntity> res = new ArrayList<>();
		for(Exercise exercise : exercises) {
			res.add(mapPojoToEntity(exercise));
		}
		return res;
	}

	public List<Exercise> entityListToPojoList(List<ExerciseEntity> exercises) {
		ArrayList<Exercise> res = new ArrayList<>();
		for(ExerciseEntity exercise : exercises) {
			res.add(mapEntityToPojo(exercise));
		}
		return res;
	}
}
