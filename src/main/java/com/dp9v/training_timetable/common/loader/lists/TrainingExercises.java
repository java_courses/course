package com.dp9v.training_timetable.common.loader.lists;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.loader.models.TrainingExerciseXML;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlRootElement(name = "TrainingExercises")
@XmlType(propOrder = {"items"})
@XmlSeeAlso(TrainingExerciseXML.class)
public class TrainingExercises extends AbstractModelList<TrainingExerciseXML> {
	public TrainingExercises(ArrayList<TrainingExerciseXML> items) {
		super(items);
	}

	public void fillTrainingExercises(List<TrainingExercise> items)
	{
		for(TrainingExercise item: items) {
			this.putItem(new TrainingExerciseXML(item));
		}
	}

	public TrainingExercises() {
		super();
	}

	public ArrayList<TrainingExercise> getModels(int begin, int count) throws ServiceException {
		ArrayList<TrainingExercise> trainingExercises = new ArrayList<>(count);
		for(int i = begin; i < begin + count && i < this.getItems().size(); i++) {
			trainingExercises.add((TrainingExercise) getItems().get(i).getModel());
		}
		return trainingExercises;
	}
}
