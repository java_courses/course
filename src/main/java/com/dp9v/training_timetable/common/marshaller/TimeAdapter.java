package com.dp9v.training_timetable.common.marshaller;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Time;

/**
 * xmlAdapter для преобразования java.sql.Time к String при маршализациии и обратно
 * <p>
 * Created by dpolkovnikov on 21.02.17.
 */
public class TimeAdapter extends XmlAdapter<String, Time> {

	/**
	 * Преобразует строку v к типу java.sql.Time
	 *
	 * @param v - Входная строка фомата hh:mm:ss
	 * @return Значение типа java.sql.Time, соответсвующее входной строке
	 */
	@Override
	public Time unmarshal(String v) throws Exception {
		return Time.valueOf(v);
	}

	/**
	 * Преобразует время v к Строке
	 *
	 * @param v объект java.sql.Time
	 * @return Строчное представление входного объекта
	 */
	@Override
	public String marshal(Time v) throws Exception {
		return v.toString();
	}
}
