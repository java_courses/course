package com.dp9v.training_timetable.common.loader.models;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.common.marshaller.DateAdapter;
import com.dp9v.training_timetable.common.marshaller.TimeAdapter;
import com.dp9v.training_timetable.models.pojo.Training;
import com.dp9v.training_timetable.services.UserService;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Date;
import java.sql.Time;
import java.util.UUID;

/**
 * Created by dpolkovnikov on 02.03.17.
 */
@XmlRootElement(name = "training")
public class TrainingXML implements XMLInterface {

	private UUID          identifier;
	private UUID          user;
	private java.sql.Date date;
	private java.sql.Time beginTime;

	public TrainingXML() {
	}

	public TrainingXML(Training training) {
		this.identifier = training.getIdentifier();
		this.user = training.getUser().getToken();
		this.date = training.getDate();
		this.beginTime = training.getBeginTime();
	}

	@Override
	public Object getModel() throws UserServiceException {
		Training res = new Training();
		res.setIdentifier(identifier);
		res.setUser(new UserService().getByToken(user));
		res.setDate(date);
		res.setBeginTime(beginTime);
		return res;
	}


	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public UUID getUser() {
		return user;
	}

	public void setUser(UUID user) {
		this.user = user;
	}

	@XmlJavaTypeAdapter(DateAdapter.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@XmlJavaTypeAdapter(TimeAdapter.class)
	public Time getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Time beginTime) {
		this.beginTime = beginTime;
	}


}
