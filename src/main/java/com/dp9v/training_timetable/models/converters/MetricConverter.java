package com.dp9v.training_timetable.models.converters;

import com.dp9v.training_timetable.models.entity.MetricEntity;
import com.dp9v.training_timetable.models.pojo.Metric;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 19.03.17.
 */
@Service
public class MetricConverter {
	private final static MapperFacade mapper;

	static {
		final MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
		mapperFactory.classMap(Metric.class, MetricEntity.class)
		             .byDefault().register();
		mapper = mapperFactory.getMapperFacade();
	}

	public MetricConverter() {
	}

	public Metric mapEntityToPojo(final MetricEntity metric) {
		Metric pDto = mapper.map(metric, Metric.class);
		return pDto;
	}

	public MetricEntity mapPojoToEntity(final Metric metric) {
		MetricEntity pDto = mapper.map(metric, MetricEntity.class);
		return pDto;
	}

	public List<MetricEntity> pojoListToEntityList(List<Metric> metrics) {
		ArrayList<MetricEntity> res = new ArrayList<>();
		for(Metric metric : metrics) {
			res.add(mapPojoToEntity(metric));
		}
		return res;
	}

	public List<Metric> entityListToPojoList(List<MetricEntity> metrics) {
		ArrayList<Metric> res = new ArrayList<>();
		for(MetricEntity metric : metrics) {
			res.add(mapEntityToPojo(metric));
		}
		return res;
	}
}
