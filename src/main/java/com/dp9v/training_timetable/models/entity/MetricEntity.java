package com.dp9v.training_timetable.models.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dpolkovnikov on 20.03.17.
 */
@Entity
@Table(name = "metrics", schema = "public", catalog = "TrainingTimetable")
public class MetricEntity {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "main_seq")
	@SequenceGenerator(name = "main_seq", sequenceName = "main_seq", allocationSize = 1)
	private Long id;

	@Column(length = 50)
	private String type;

	@Column(name = "delete_type", insertable = false)
	private Integer deleteType;

	@Column
	@Version
	private Integer version;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "metric")
	private Set<ExerciseEntity> exercises = new HashSet<>();

	public MetricEntity() {
	}

	public Set<ExerciseEntity> getExercises() {
		return exercises;
	}

	public void setExercises(Set<ExerciseEntity> exercises) {
		this.exercises = exercises;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}
