package com.dp9v.training_timetable.common.loader;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.common.loader.lists.*;
import com.dp9v.training_timetable.common.loader.models.*;
import com.dp9v.training_timetable.models.pojo.*;
import com.dp9v.training_timetable.services.interfaces.ServiceInterface;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 * Created by dpolkovnikov on 25.02.17.
 */
public class Loader implements Callable<Integer> {
	private static Logger logger = Logger.getLogger(Loader.class);
	private ServiceInterface  service;
	private AbstractModelList list;

	public Loader(AbstractModelList list, ServiceInterface service) {
		this.service = service;
		this.list = list;
	}

	@Override
	public Integer call() {
		if(list instanceof Users) {
			return loadUsers((Users) list, list.size() / 2 + 1);
		} else if(list instanceof Metrics) {
			return loadMetrics((Metrics) list, list.size() / 2 + 1);
		} else if(list instanceof Exercises) {
			return loadExercises((Exercises) list, list.size() / 5 + 1);
		} else if(list instanceof Trainings) {
			return loadTrainings((Trainings) list, list.size() / 5 + 1);
		} else if(list instanceof TrainingExercises) {
			return loadTrainingExercises((TrainingExercises) list, list.size() / 10 + 1);
		} else {
			logger.error("Необходимая таблица не обнаружена");
			return 0;
		}
	}

	private int loadUsers(Users users, int batchSize) {
		ArrayList<UserXML> badUsers = new ArrayList<>();
		ArrayList<User>    load     = new ArrayList<>();
		int                start    = 0;
		while(start < users.size()) {
			try {
				load = users.getModels(start, batchSize);
				service.insertAll(load);
			}
			catch (ServiceException e) {
				badUsers.addAll(users.subList(start, batchSize));
			}
			load.clear();
			start += batchSize;
		}
		users.setItems(badUsers);
		return badUsers.size();
	}

	private int loadMetrics(Metrics metrics, int batchSize) {
		ArrayList<MetricXML> badMetrics = new ArrayList<>();
		ArrayList<Metric>    load       = new ArrayList<>();
		int                  start      = 0;
		while(start < metrics.size()) {
			try {
				load = metrics.getModels(start, batchSize);
				service.insertAll(load);
			}
			catch (ServiceException e) {
				badMetrics.addAll(metrics.subList(start, batchSize));
			}
			load.clear();
			start += batchSize;
		}
		metrics.setItems(badMetrics);
		return badMetrics.size();
	}

	private int loadExercises(Exercises exercises, int batchSize) {
		ArrayList<ExerciseXML> badExercises = new ArrayList<>();
		ArrayList<Exercise>    load         = new ArrayList<>();
		int                    start        = 0;
		while(start < exercises.size()) {
			try {
				load = exercises.getModels(start, batchSize);
				service.insertAll(load);
			}
			catch (ServiceException e) {
				badExercises.addAll(exercises.subList(start, batchSize));
			}
			load.clear();
			start += batchSize;
		}
		exercises.setItems(badExercises);
		return badExercises.size();
	}

	private int loadTrainings(Trainings trainings, int batchSize) {
		ArrayList<TrainingXML> badTrainings = new ArrayList<>();
		ArrayList<Training>    load         = new ArrayList<>();
		int                    start        = 0;
		while(start < trainings.size()) {
			try {
				load = trainings.getModels(start, batchSize);
				service.insertAll(load);
			}
			catch (ServiceException e) {
				badTrainings.addAll(trainings.subList(start, batchSize));
			}
			load.clear();
			start += batchSize;
		}
		trainings.setItems(badTrainings);
		return badTrainings.size();
	}

	private int loadTrainingExercises(TrainingExercises trainingExercises, int batchSize) {
		ArrayList<TrainingExerciseXML> badTrainingExercises = new ArrayList<>();
		ArrayList<TrainingExercise>    load                 = new ArrayList<>();
		int                            start                = 0;
		while(start < trainingExercises.size()) {
			try {
				load = trainingExercises.getModels(start, batchSize);
				service.insertAll(load);
			}
			catch (ServiceException e) {
				badTrainingExercises.addAll(trainingExercises.subList(start, batchSize));
			}
			load.clear();
			start += batchSize;
		}
		trainingExercises.setItems(badTrainingExercises);
		return badTrainingExercises.size();
	}
}
