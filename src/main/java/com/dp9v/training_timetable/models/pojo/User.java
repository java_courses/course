package com.dp9v.training_timetable.models.pojo;


import java.sql.Date;
import java.util.UUID;

public class User {

	private Long    id;
	private UUID    identifier;
	private String  name;
	private String  email;
	private String  password;
	private Date    birthday;
	private Boolean isAdmin;
	private Boolean isActive;
	private Boolean getNotifications;
	private UUID    token;
	private Integer deleteType;
	private Integer version;

	public User(Long id, UUID identifier, String name, String email, String password, Date birthday, Boolean isAdmin,
	            Boolean isActive, Boolean getNotifications, UUID token) {
		this.id = id;
		this.identifier = identifier;
		this.name = name;
		this.setEmail(email);
		this.password = password;
		this.birthday = birthday;
		this.isAdmin = isAdmin;
		this.isActive = isActive;
		this.getNotifications = getNotifications;
		this.token = token;
	}

	public User() {
		this.isAdmin = false;
		this.isActive = false;
		this.getNotifications = false;
	}

	public User(String name, String email, String password, Date birthday) {
		this.name = name;
		this.setEmail(email);
		this.password = password;
		this.birthday = birthday;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean admin) {
		isAdmin = admin;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean active) {
		isActive = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public Boolean getGetNotifications() {
		return getNotifications;
	}

	public void setGetNotifications(Boolean notif) {
		this.getNotifications = notif;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String login) {
		this.email = login.toLowerCase();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public java.sql.Date getBirthday() {
		return birthday;
	}

	public void setBirthday(java.sql.Date birthday) {
		this.birthday = birthday;
	}


}
