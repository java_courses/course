package com.dp9v.training_timetable.forms;

import com.dp9v.training_timetable.models.pojo.Exercise;
import com.dp9v.training_timetable.models.pojo.Training;
import com.dp9v.training_timetable.models.pojo.TrainingExercise;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public class TrainingExerciseForm {

	private Long id;

	@NotNull(message = "Не указана тренировка")
	private Long trainingfk;

	@NotNull(message = "Не выбрано упражнение")
	private Long exercisefk;

	private Double counter;

	private String comment;

	@Min(value = 0, message = "Слишком маленькая версия")
	private Integer version;

	public TrainingExerciseForm() {
	}

	public Long getTrainingfk() {
		return trainingfk;
	}

	public void setTrainingfk(Long trainingfk) {
		this.trainingfk = trainingfk;
	}

	public Long getExercisefk() {
		return exercisefk;
	}

	public void setExercisefk(Long exercisefk) {
		this.exercisefk = exercisefk;
	}

	public Double getCounter() {
		return counter;
	}

	public void setCounter(Double counter) {
		this.counter = counter;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public TrainingExercise getModel() {
		TrainingExercise res = new TrainingExercise();
		res.setId(id);
		res.setVersion(version);
		res.setComment(comment);
		res.setCounter(counter);
		Training training = new Training();
		training.setId(trainingfk);
		res.setTraining(training);
		Exercise exercise = new Exercise();
		exercise.setId(exercisefk);
		res.setExercise(exercise);
		return res;
	}

}
