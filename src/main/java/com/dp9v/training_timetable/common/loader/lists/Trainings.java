package com.dp9v.training_timetable.common.loader.lists;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.UserServiceException;
import com.dp9v.training_timetable.common.loader.models.TrainingXML;
import com.dp9v.training_timetable.models.pojo.Training;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpolkovnikov on 21.02.17.
 */
@XmlRootElement(name = "Trainings")
@XmlType(propOrder = {"items"})
@XmlSeeAlso(TrainingXML.class)
public class Trainings extends AbstractModelList<TrainingXML> {
	public Trainings(ArrayList<TrainingXML> items) {
		super(items);
	}

	public Trainings() {
	}

	public void fillTrainings(List<Training> items) {
		for(Training item : items) {
			this.putItem(new TrainingXML(item));
		}
	}

	public ArrayList<Training> getModels(int begin, int count) throws UserServiceException {
		ArrayList<Training> trainings = new ArrayList<>(count);
		for(int i = begin; i < begin + count && i < this.getItems().size(); i++) {
			trainings.add((Training) getItems().get(i).getModel());
		}
		return trainings;
	}
}
