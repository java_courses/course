package com.dp9v.training_timetable.forms;

import com.dp9v.training_timetable.models.pojo.Training;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by dpolkovnikov on 21.03.17.
 */
public class TrainingForm {

	private long id;

	@NotNull(message = "Не указана дата тренировки")
	private Date date;

	private Time beginTime;

	@Min(value = 0, message = "Слишком маленькая версия")
	private int version;

	public TrainingForm() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(String date) {
		if(date == null || "".equals(date))
			this.date = null;
		else
			this.date = Date.valueOf(date);
	}

	public Time getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		if(beginTime == null || "".equals(beginTime))
			this.beginTime = null;
		else
			this.beginTime = Time.valueOf(beginTime + ":00");
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Training getModel(){
		Training res = new Training();
		res.setId(getId());
		res.setVersion(getVersion());
		res.setBeginTime(getBeginTime());
		res.setDate(getDate());
		return res;
	}
}
