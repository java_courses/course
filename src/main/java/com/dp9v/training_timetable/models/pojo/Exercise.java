package com.dp9v.training_timetable.models.pojo;

import java.util.UUID;

public class Exercise {

	public  long    id;
	private UUID    identifier;
	private String  name;
	private String  info;
	private Metric  metric;
	private Integer deleteType;
	private Integer version;

	public Exercise() {
	}

	public Exercise(long id, UUID identifier, String name, String info, Metric metric) {
		this.id = id;
		this.identifier = identifier;
		this.name = name;
		this.info = info;
		this.metric = metric;
	}

	public Exercise(UUID identifier, String name, String info, Metric metric) {
		this.identifier = identifier;
		this.name = name;
		this.info = info;
		this.metric = metric;
	}

	public Exercise(long id, String name, String info, Metric metric) {
		this.id = id;
		this.name = name;
		this.info = info;
		this.metric = metric;
	}

	public Integer getDeleteType() {
		return deleteType;
	}

	public void setDeleteType(Integer deleteType) {
		this.deleteType = deleteType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UUID getIdentifier() {
		return identifier;
	}

	public void setIdentifier(UUID identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}


	public Metric getMetric() {
		return metric;
	}

	public void setMetric(Metric metric) {
		this.metric = metric;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
