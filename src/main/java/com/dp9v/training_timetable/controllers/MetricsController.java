package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.common.exceptions.serviceexceptions.MetricServiceException;
import com.dp9v.training_timetable.common.exceptions.serviceexceptions.ServiceException;
import com.dp9v.training_timetable.forms.MetricForm;
import com.dp9v.training_timetable.models.pojo.Metric;
import com.dp9v.training_timetable.services.interfaces.IMetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dpolkovnikov on 10.03.17.
 */
@Controller
@RequestMapping(value = "/admin")
public class MetricsController {
	IMetricService metricService;

	@Autowired
	public void setMetricService(IMetricService metricService) {
		this.metricService = metricService;
	}

	@ResponseBody
	@RequestMapping(value = "/removeMetric", method = RequestMethod.POST)
	public Map<String, Object> removeMetric(@RequestParam("id") long id) {
		Map<String, Object> res = new HashMap<>(4);
		try {
			metricService.delete(id);
			res.put("success", true);
			res.put("id", id);
		}
		catch (ServiceException e) {
			res.put("message", e.getMessage());
			res.put("success", false);
		}
		return res;
	}

	@ResponseBody
	@RequestMapping(value = "/saveMetric", method = RequestMethod.POST)
	public Map<String, Object> saveMetric(@Valid MetricForm form, BindingResult bindingResult) {
		Map<String, Object> res = new HashMap<>(4);
		if(bindingResult.hasErrors()) {
			res.put("success", false);
			res.put("message", bindingResult.getFieldError().getDefaultMessage());
		} else {
			try {
				Metric model = form.getModel();
				model = metricService.save(model);
				res.put("success", true);
				res.put("object", model);
			}
			catch (MetricServiceException e) {
				res.put("success", false);
				res.put("message", e.getMessage());
			}
		}
		return res;
	}
}
