package com.dp9v.training_timetable.controllers;

import com.dp9v.training_timetable.repositories.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by dpolkovnikov on 05.03.17.
 */
@Controller("export")
@RequestMapping("export")
public class ExportController {
	String[] TABLES   = {MetricRepository.TABLE_NAME,
	                     UserRepository.TABLE_NAME,
	                     TrainingRepository.TABLE_NAME,
	                     ExerciseRepository.TABLE_NAME,
	                     TrainingExerciseRepository.TABLE_NAME};
	String   DIR_PATH = "/home/dpolkovnikov/Projects/java/courses/cource/.exports/";

//	@RequestMapping(value = "/export")
//	public String startExport() {
//		Executor executor = new Executor();
//		executor.runExporters(TABLES, DIR_PATH);
//		return "error";
//	}
//
//	@RequestMapping(value = "/import")
//	public String startImport() {
//		Executor executor = new Executor();
//		String[] FILES    = new String[TABLES.length];
//		for(int i = 0; i < TABLES.length; i++) {
//			Path path = Paths.get(DIR_PATH, TABLES[i] + ".xml");
//			FILES[i] = path.toString();
//		}
//		executor.runImporters(FILES);
//		return "error";
//	}
}
